package green.team.v2.com.petcare.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.R;

public class PetCareViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = " PetViewHolder";
    private static final String ERROR_MSG = "Oops, Something went wrong. ";

    public TextView business_name;
    public TextView category;
    public TextView totalReview;
    public RatingBar avrgRating;
    private View view;

    public PetCareViewHolder(@NonNull View itemView) {
        super(itemView);

        business_name = itemView.findViewById(R.id.businessTV);
        category = itemView.findViewById(R.id.serviceCat);
        totalReview = itemView.findViewById(R.id.totalReviews);
        avrgRating = itemView.findViewById(R.id.avrgRating);
        view = itemView;
    }

    public void setPhoto(final String photoUrl){
        final ImageView coverPhoto = view.findViewById(R.id.petcareIV);

        Picasso.get().load(photoUrl)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                .fit().into(coverPhoto,
                new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Success loading image.");
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(photoUrl).fit().into(coverPhoto);
                        Log.e(TAG, ERROR_MSG + e);
                    }
                });
    }

}
