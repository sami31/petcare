package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.models.Pet;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.User;
import green.team.v2.com.petcare.viewholder.PetViewHolder;
import green.team.v2.com.petcare.viewholder.ServiceViewHolder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import javax.annotation.Nullable;

public class PersonalInfoActivity extends AppCompatActivity {

    private static final String TAG = "PersonalInfoActivity";
    private static final String Error_Msg = "Oops, Something went wrong :( ";
    public static String EXTRA_USER_ID = "user_key";

    private String user_id;
    private String username;
    private String CUID;

    private FirestoreRecyclerAdapter<Pet, PetViewHolder> petAdapter;
    private FirestoreRecyclerAdapter<PetCareProvider, ServiceViewHolder> serviceAdapter;

    private GridLayoutManager petManager;

    private RecyclerView petRV;

    private FirebaseFirestore db;

    private TextView personalName;
    private TextView personalContact;
    private TextView personalEmail;
    private TextView personalAddress;
    private TextView petResultsTV;
    private CircleImageView personalPhoto;
    private CircleImageView personalStatus;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        db = FirebaseFirestore.getInstance();
        user_id = getIntent().getStringExtra(EXTRA_USER_ID);

        petManager = new GridLayoutManager(this, 2);
        petRV = findViewById(R.id.personalPetRV);
        petRV.setHasFixedSize(true);
        petRV.setItemAnimator(new DefaultItemAnimator());
        petRV.setLayoutManager(petManager);

        Toolbar personalToolbar = findViewById(R.id.toolbarPersonal);
        setSupportActionBar(personalToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        personalName = findViewById(R.id.personalName);
        personalContact = findViewById(R.id.personalContact);
        personalEmail = findViewById(R.id.personalEmail);
        personalAddress = findViewById(R.id.personalAddress);
        personalPhoto = findViewById(R.id.personalPhoto);
        petResultsTV = findViewById(R.id.petResultsTV);
        personalStatus = findViewById(R.id.personalStatus);
        petResultsTV.setVisibility(View.GONE);

        getPersonalInfo();
        getPets();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            CUID = currentUser.getUid();
        }
    }

    private void getPersonalInfo(){
        DocumentReference ref =  db.collection("users").document(user_id);
        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if(e == null && documentSnapshot.exists()){

                    User user = documentSnapshot.toObject(User.class);
                    username = user.username;
                    actionBar.setTitle(username);
                    personalName.setText(username);
                    personalContact.setText(user.contact_no);
                    personalAddress.setText(user.user_address);
                    personalEmail.setText(user.email);
                    Picasso.get().load(user.image_url)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit().into(personalPhoto);

                    if (user.isOnline){
                        personalStatus.setBackground(ContextCompat
                                .getDrawable(PersonalInfoActivity.this, R.drawable.online_status));
                    }else{
                        personalStatus.setBackground(ContextCompat
                                .getDrawable(PersonalInfoActivity.this, R.drawable.offline_status));

                    }

                }else if (e != null){

                    Log.e(TAG, Error_Msg + e);
                }
            }
        });
    }

    private void getPets(){
        Query petQuery = db.collection("pets").whereEqualTo("owner_id", user_id);

        getResults(petQuery);

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<Pet>()
                .setQuery(petQuery, Pet.class)
                .setLifecycleOwner(this)
                .build();

        petAdapter = new FirestoreRecyclerAdapter<Pet, PetViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final PetViewHolder petViewHolder, int i, @NonNull final Pet pet) {

                final String pet_id = getSnapshots().getSnapshot(i).getId();
                petViewHolder.petNameView.setText(pet.petName);
                String petUrl = pet.petImg;
                petViewHolder.setPetImgView(petUrl);
                petViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PersonalInfoActivity.this, ViewPetActivity.class);
                        intent.putExtra(ViewPetActivity.EXTRA_PET_ID, pet_id);
                        startActivity(intent);
                        finish();
                    }
                });

                Query likesRef = db.collection("pets").document(pet_id).collection("likes");

                likesRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e) {

                    if (queryDocumentSnapshots != null && e == null){
                        int likeCount = 0;
                        for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots){
                            likeCount++;
                            if (snapshot.getBoolean("isLike")) {
                                String count;
                                if (likeCount > 1){
                                    count = String.valueOf(likeCount) + " likes";
                                }else {
                                    count = String.valueOf(likeCount) + " like";
                                }
                                petViewHolder.likes.setText(count);
                            }else {
                                likeCount--;
                                String count;
                                if (likeCount > 1){
                                    count = String.valueOf(likeCount) + " likes";
                                }else {
                                    count = String.valueOf(likeCount) + " like";
                                }
                                petViewHolder.likes.setText(count);
                            }

                        }
                    }
                    }
                });


                petViewHolder.likeButton.setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        updateLikes(pet_id, true);
                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        updateLikes(pet_id, false);
                    }
                });

                DocumentReference petRef = db.collection("pets").document(pet_id)
                        .collection("likes").document(CUID);

                petRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null && e == null){
                            if (documentSnapshot.getBoolean("isLike" )!= null) {

                                if (documentSnapshot.getBoolean("isLike")) {

                                    petViewHolder.likeButton.setLiked(true);
                                } else {

                                    petViewHolder.likeButton.setLiked(false);
                                }
                            }
                        }
                    }
                });

            }

            @NonNull
            @Override
            public PetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new PetViewHolder(inflater.inflate(R.layout.item_pets, parent, false));
            }

        };

        petRV.setAdapter(petAdapter);

    }

    private void updateLikes(String pet_id, Boolean like){
        final DocumentReference petRef = db.collection("pets").document(pet_id)
                .collection("likes").document(CUID);
        Log.e(TAG, Error_Msg + pet_id);

        HashMap<String, Object> likeMap = new HashMap<>();
        likeMap.put("isLike", like);
        petRef.set(likeMap);

    }

    private void getResults(Query q){

        q.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {
                if ( queryDocumentSnapshots.isEmpty() && e == null ){
                    String[] name = username.split(" ");
                    String result = name[0] + " " + "is rather lazy to add his/her pets.";
                    petResultsTV.setText(result);
                    petResultsTV.setVisibility(View.VISIBLE);

                }else if ( !queryDocumentSnapshots.isEmpty() && e == null ){

                    petResultsTV.setVisibility(View.GONE);

                }else if( e != null ){

                    petResultsTV.setText(Error_Msg);

                }
            }
        });

    }
}
