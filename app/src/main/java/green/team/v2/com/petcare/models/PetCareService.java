package green.team.v2.com.petcare.models;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class PetCareService {

    public String serviceCategory;
    public String service_rate;
    public String service_details;

    public PetCareService() {
    }

    public PetCareService(String serviceCategory, String service_rate, String service_details) {

        this.serviceCategory    = serviceCategory;
        this.service_rate       = service_rate;
        this.service_details    = service_details;
    }
}
