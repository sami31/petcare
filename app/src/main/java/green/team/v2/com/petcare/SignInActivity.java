package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.models.User;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.shobhitpuri.custombuttons.GoogleSignInButton;
import com.squareup.picasso.Picasso;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Sign-In Activity";
    private static final String Error_Msg = "Something went wrong.";
    private static final int RC_SIGN_IN = 1234;

    private ImageView petcareIV;

    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInButton mSignInButton;
    private LoginButton fbBtn;
    private FirebaseAuth mAuth;

    private FirebaseFirestore db;

    private CallbackManager callbackManager;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        petcareIV = findViewById(R.id.petcarewordIV);
        Picasso.get().load(R.drawable.ic_petcare_word).fit().into(petcareIV);

        db = FirebaseFirestore.getInstance();

        mSignInButton = findViewById(R.id.signInBtn);
        fbBtn = findViewById(R.id.fbBtn);

        mSignInButton.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
        fbBtn.setReadPermissions("email", "public_profile");
        fbSignIn();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Log.d(TAG, "Getting Google client.");

        mAuth = FirebaseAuth.getInstance();

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Authenticating...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (getAgreementStatus()) {
            disclaimer(true);
        }else{
            disclaimer(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signInBtn:
                googleSignIn();
                break;
        }
    }

    private void fbSignIn(){
        fbBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mProgress.show();
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                firebaseAuthwithFB(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, Error_Msg + "Cancelled.");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, Error_Msg + error);
            }
        });
    }

    private void googleSignIn() {
        try {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
            Log.d(TAG, "Sign in Intent.");
        }catch(Exception e){
            Log.e(TAG, Error_Msg + e);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                mProgress.show();
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.e(TAG,Error_Msg + e);
                Toast.makeText(this, Error_Msg + e, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthwithFB(AccessToken token){
        Log.d(TAG, "FacebookAccessToken:" + token.getLastRefresh());
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        if(mAuth.getCurrentUser()== null) {
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                mProgress.dismiss();
                                FirebaseUser user = mAuth.getCurrentUser();
                                setValueEventListener(user);
                                Toast.makeText(SignInActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            } else {
                                mProgress.dismiss();
                                Log.e(TAG, Error_Msg + task.getException());
                                Toast.makeText(SignInActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }else{
            mProgress.dismiss();
            Toast.makeText(this, "You are signed in.", Toast.LENGTH_SHORT).show();
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG,"Sign in successful.");
                            FirebaseUser user = mAuth.getCurrentUser();
                            setValueEventListener(user);
                        } else {
                            // If sign in fails, display a message to the user.
                          Log.e(TAG,Error_Msg + task.getResult());
                        }
                    }
                });
    }

    private void setValueEventListener(final FirebaseUser user){
        String UID = user.getUid();
        final DocumentReference userRef =  db.collection("users").document(UID);
        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()){
                    mProgress.dismiss();
                    startSplashActivity();
                }else{
                    mProgress.dismiss();
                    openDialog(user, userRef);
                }
            }
        });
    }

    private void createUserInFirebase(FirebaseUser firebaseUser, DocumentReference reference){

        mProgress.show();

        final String name = firebaseUser.getDisplayName();
        final String email = firebaseUser.getEmail();
        final String contact_no = "No contact no. provided yet.";
        final String image_url = firebaseUser.getPhotoUrl().toString();
        final String address = "No address provided yet.";

        User user = new User(name, email, contact_no, image_url, address, true);
        reference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "You have successfully created an account");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, Error_Msg + e);
            }
        });

    }

    private void startSplashActivity() {

        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }

    private void openDialog(final FirebaseUser user, final DocumentReference userRef){

        CharSequence options[] = new CharSequence[]{
                "Pet Owner",
                "PetCare Provider"
        };

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Sign in as :")
                        .setCancelable(false);
        dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0){
                    createUserInFirebase(user, userRef);
                    startSplashActivity();
                    dialogInterface.dismiss();
                }else {
                    startActivity(new Intent( SignInActivity.this, AddServicesActivity.class));
                    finish();
                    dialogInterface.dismiss();
                }
           }
       });

        dialogBuilder.show();
    }

    private void disclaimer(boolean hide){
        // Build an AlertDialog
        final Dialog dialog = new Dialog(SignInActivity.this);

        // Set the custom layout as alert dialog view
        dialog.setContentView(R.layout.popup_disclaimer);
        dialog.setCancelable(false);

        // Get the custom alert dialog view widgets reference
        TextView policyTV = dialog.findViewById(R.id.privacyTV);
        TextView cancelTV = dialog.findViewById(R.id.cancelBtn);
        final TextView continueTV = dialog.findViewById(R.id.continueBtn);
        CheckBox agreeCB = dialog.findViewById(R.id.agreeCB);
        continueTV.setVisibility(View.INVISIBLE);

        //Set on Check change listener for checkbox
       agreeCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               Boolean checked = (compoundButton).isChecked();
               if (checked){
                   storeAgreementStatus(true);
                   continueTV.setVisibility(View.VISIBLE);
               }else{
                   storeAgreementStatus(false);
                   continueTV.setVisibility(View.INVISIBLE);
               }
           }
       });

        // Create the alert dialog
        dialog.create();

        continueTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        policyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, PrivacyPolicyActivity.class));
            }
        });

       if (hide){
           dialog.dismiss();
       }else{
           dialog.show();
       }
    }

    private void storeAgreementStatus(boolean isChecked){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("agree", isChecked);
        mEditor.apply();
    }

    private boolean getAgreementStatus(){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("agree", false);
    }

}
