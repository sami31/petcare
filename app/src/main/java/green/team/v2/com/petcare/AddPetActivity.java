package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.models.Pet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AddPetActivity extends AppCompatActivity {

    private static final String TAG = "Pets Activity";
    private static final String Error_Msg = "Something went wrong.";
    private final int PICK_IMAGE_REQUEST = 31;

    private Spinner sizeSpinner;
    private Spinner kindSpinner;
    private FloatingActionButton savePetBtn;
    private EditText petNameText;
    private EditText petBreedText;
    private EditText petAboutET;

    private ImageButton petImgBtn;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser firebaseUser;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private String petImg;
    private Uri filePath;
    private String UID;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pets_add);

        db = FirebaseFirestore.getInstance();

        String[] petSize = new String[]{"Small", "Medium", "Large"};
        sizeSpinner = findViewById(R.id.sizeSpinner);
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_dropdown_item, petSize );
        sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeSpinner.setSelected(false);
        sizeSpinner.setAdapter(sizeAdapter);

        String[] petKind = new String[]{"Dog", "Cat", "Rabbit", "Guines Pig", "Bird", "Hamster", "Others"};
        kindSpinner = findViewById(R.id.kindSpinner);
        ArrayAdapter<String> kindAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_dropdown_item, petKind );
        kindAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kindSpinner.setSelected(false);
        kindSpinner.setAdapter(kindAdapter);

        petNameText = findViewById(R.id.petName);
        petBreedText = findViewById(R.id.petBreed);
        petAboutET = findViewById(R.id.petAboutET);
        petImgBtn = findViewById(R.id.petImg);

        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        savePetBtn = findViewById(R.id.savePetBtn);
        savePetBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 checkfields();
             }
         });

        petImgBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 chooseImg();
             }
         });

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Saving...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

    }

    private void checkfields() {

        String petName = petNameText.getText().toString();
        String petType = kindSpinner.getSelectedItem().toString();
        String petBreed = petBreedText.getText().toString();
        String petSize = sizeSpinner.getSelectedItem().toString();
        String petAbout = petAboutET.getText().toString();

        String values[] = new String[]{petName, petBreed, petAbout};
        EditText fields[] = new EditText[]{petNameText, petBreedText, petAboutET};

        for (int i = 0; i < values.length; i++) {
            if (values[i].isEmpty()){

                fields[i].setError("Required");
                Toast.makeText(this, "Please fill up required fields."
                        , Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (petImg.isEmpty()){
            Toast.makeText(this, "Please pick add photo of your pet."
                    , Toast.LENGTH_SHORT).show();
            return;
        }

        savePet(petName, petType, petBreed, petSize, petAbout);
    }

    private void savePet(String petName, String petType, String petBreed, String petSize, String petAbout){

        if (petImg == null){
            Toast.makeText(this, "Please add a photo of your pet",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mProgress.show();
        UID = firebaseUser.getUid();
        Pet pet = new Pet(UID, petName, petType, petBreed, petSize, petImg, petAbout);

        CollectionReference petRef = db.collection("pets");

        petRef.add(pet).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if (task.isSuccessful()){
                    mProgress.dismiss();
                    Toast.makeText(AddPetActivity.this, "You have successfully added a pet."
                            , Toast.LENGTH_SHORT).show();
                    startActivity( new Intent(AddPetActivity.this, MyPetsActivity.class));
                    finish();
                }else{
                    mProgress.dismiss();
                    Log.e(TAG, Error_Msg + task.getException());
                    Toast.makeText(AddPetActivity.this, Error_Msg
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void chooseImg(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null
                && data.getData() != null ) {
            try {
                filePath = data.getData();
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                petImgBtn.setImageBitmap(bmp);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
            }catch(IOException e){
                Log.e(TAG, Error_Msg + e);
            }
        }
    }

    private void uploadImg(final byte[] bytedata) {
        String petName = petNameText.getText().toString();

        if(filePath != null && firebaseUser != null) {
            UID = firebaseUser.getUid();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("pet_image/" + UID + "/" + petName + ".jpg");
            UploadTask uploadTask = ref.putBytes(bytedata);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(AddPetActivity.this, "Photo uploaded successfully"
                            , Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage("Upload is " + progress + "% done");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Log.e(TAG, Error_Msg + e);
                    Toast.makeText(AddPetActivity.this, Error_Msg
                            , Toast.LENGTH_SHORT).show();
                }
            });

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        petImg = downloadUri.toString();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Log.e(TAG, Error_Msg + e);
                    Toast.makeText(AddPetActivity.this, Error_Msg
                            , Toast.LENGTH_SHORT).show();
                }
            });

        }

    }
}
