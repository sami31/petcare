package green.team.v2.com.petcare;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

public class SplashActivity extends AppCompatActivity {

    private final String TAG = "SplashActivity";
    private NetworkInfo activeNetwork;
    private Boolean isConnected;
    private boolean gps_enabled;
    private String UID;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        db = FirebaseFirestore.getInstance();

        ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnected();

        LocationManager lm = (LocationManager) getBaseContext().getSystemService(Context.LOCATION_SERVICE);
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(isConnected) {
            if(gps_enabled) {
                checkCurrentUser();
            } else {
                noLocationDialog();
            }

        } else {

            noNetworkDialog();
        }

    }

    private void checkCurrentUser(){

        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();


        if(currentUser != null){
            UID = currentUser.getUid();
            checkUserIfExist();
            Log.d(TAG,"starting main activity...");
        }else{
            startActivity(new Intent(this,SignInActivity.class));
            finish();
        }
    }

    private void checkUserIfExist(){
        DocumentReference reference = db.collection("users").document(UID);
        reference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()){
                    startMainActivity();
                }else{
                    startActivity(new Intent(SplashActivity.this,SignInActivity.class));
                    finish();
                }
            }
        });

    }

    private void startMainActivity(){

        int Splash_time_out = 3000;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(homeIntent);
                finish();
            }
        },Splash_time_out);
    }

    private void noNetworkDialog() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Houston we have a problem.");
        dialogBuilder.setMessage("No internet.");

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        dialogBuilder.show();

    }

    private void noLocationDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Sorry..");
        dialogBuilder.setMessage("Please turn on location settings" +
                "and set to high accuracy before continuing.");

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        dialogBuilder.show();
    }


}
