package green.team.v2.com.petcare.models;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Rating {

    public String rater;
    public String rating;
    public String review;
    public String reviewer_photo;

    public Rating() {
    }

    public Rating(String rater, String rating, String review, String reviewer_photo) {

        this.rater          = rater;
        this.rating         = rating;
        this.review         = review;
        this.reviewer_photo = reviewer_photo;
    }

    @Exclude
    public Map<String,Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("rater", rater);
        result.put("rating", rating);
        result.put("review", review);
        return result;
    }

}
