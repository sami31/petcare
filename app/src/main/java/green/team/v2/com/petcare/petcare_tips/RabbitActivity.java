package green.team.v2.com.petcare.petcare_tips;

import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.R;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class RabbitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rabbit);

        ImageView rabbitIV = findViewById(R.id.rabbitIV);

        rabbitIV.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.rabbit_care));
    }
}
