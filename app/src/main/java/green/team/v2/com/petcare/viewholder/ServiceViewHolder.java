package green.team.v2.com.petcare.viewholder;

import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.R;

public class ServiceViewHolder extends RecyclerView.ViewHolder {

    public TextView serviceCategory;
    public TextView serviceDetails;
    public TextView service_rate;
    public MaterialButton deleteBtn;

    public ServiceViewHolder(@NonNull View itemView) {
        super(itemView);

       serviceCategory = itemView.findViewById(R.id.serviceCatTV);
       serviceDetails = itemView.findViewById(R.id.detailsTV);
       service_rate = itemView.findViewById(R.id.service_rateTv);
       deleteBtn = itemView.findViewById(R.id.deleteServiceBtn);

    }
}
