package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.models.Pet;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class EditPetActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Pets Activity";
    private static final String Error_Msg = "Something went wrong.";
    private final int PICK_IMAGE_REQUEST = 31;
    public static final String EXTRA_PET_ID = "pet_id";
    private androidx.appcompat.app.ActionBar actionBar;

    private String UID;
    private Uri filePath;
    private String petImg;
    private String pet_id;

    private FirebaseFirestore db;
    private StorageReference storageReference;

    private ImageView editPhoto;
    private EditText editName, editBreed, editAbout;
    private TextView editType, editSize;
    private MaterialButton button;

    private String[] types;
    private String[] sizes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pet);

        db = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        pet_id = getIntent().getStringExtra(EXTRA_PET_ID);

        Toolbar toolbar = findViewById(R.id.editPetToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        types = getResources().getStringArray(R.array.service_pettype);
        sizes = new String[]{"Small", "Medium", "Large"};

        editPhoto   = findViewById(R.id.editPetPhoto);
        editName    = findViewById(R.id.editPetName);
        editType    = findViewById(R.id.editPetType);
        editBreed   = findViewById(R.id.editPetBreed);
        editSize    = findViewById(R.id.editPetSize);
        editAbout   = findViewById(R.id.editPetAbout);
        button      = findViewById(R.id.editPetBtn);

        editPhoto.setOnClickListener(this);
        editType.setOnClickListener(this);
        editSize.setOnClickListener(this);
        button.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        if ( user != null ) {
            UID = user.getUid();
            getPetInfo();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.editPetPhoto:
                chooseImg();
                break;

            case R.id.editPetType:
                choices(types, editType, "type");
                break;

            case R.id.editPetSize:
                choices(sizes, editSize, "size");
                break;

            case R.id.editPetBtn:
                updatePet();
                break;
        }
    }

    private void chooseImg(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null
                && data.getData() != null ) {
            try {
                filePath = data.getData();
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
               editPhoto.setImageBitmap(bmp);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
            }catch(IOException e){
                Log.e(TAG, Error_Msg + e);
            }
        }
    }

    private void uploadImg(final byte[] bytedata) {

        String petName = editName.getText().toString();

        if(filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("pet_image/" + UID + "/" + petName + ".jpg");
            UploadTask uploadTask = ref.putBytes(bytedata);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    progressDialog.dismiss();
                    Toast.makeText(EditPetActivity.this, "Photo uploaded successfully"
                            , Toast.LENGTH_SHORT).show();

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                    double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage("Upload is " + progress + "% done");

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    progressDialog.dismiss();
                    Log.e(TAG, Error_Msg + e);
                    Toast.makeText(EditPetActivity.this, Error_Msg
                            , Toast.LENGTH_SHORT).show();

                }
            });

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {

                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        petImg = downloadUri.toString();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    progressDialog.dismiss();
                    Log.e(TAG, Error_Msg + e);
                    Toast.makeText(EditPetActivity.this, Error_Msg
                            , Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

    private void getPetInfo(){

        DocumentReference ref = db.collection("pets").document(pet_id);

        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null && e == null){
                    Pet pet = documentSnapshot.toObject(Pet.class);
                    editName.setText(pet.petName);
                    editType.setText(pet.petType);
                    editBreed.setText(pet.petBreed);
                    editSize.setText(pet.petSize);
                    editAbout.setText(pet.petAbout);
                    petImg = pet.petImg;
                    Picasso.get().load(pet.petImg)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit()
                            .into(editPhoto);
                }else{
                    Log.e(TAG, Error_Msg + e);

                }
            }
        });
    }

    private void choices(final String[] choices, final TextView editText, String title){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Choose pet " + title)
                .setItems(choices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        editText.setText(choices[i]);
                    }
                });

        builder.create();
        builder.show();

    }

    private void updatePet(){

        String name     = editName.getText().toString().trim();
        String type     = editType.getText().toString().trim();
        String breed    = editBreed.getText().toString().trim();
        String size     = editSize.getText().toString().trim();
        String about    = editAbout.getText().toString().trim();

       HashMap<String, Object> petUpdate = new HashMap<>();
       petUpdate.put("owner_id", UID);
       petUpdate.put("petName", name);
       petUpdate.put("petType", type);
       petUpdate.put("petBreed", breed);
       petUpdate.put("petSize", size);
       petUpdate.put("petImg", petImg);
       petUpdate.put("petAbout", about);

       DocumentReference petRef = db.collection("pets").document(pet_id);
       petRef.update(petUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
           @Override
           public void onComplete(@NonNull Task<Void> task) {
               if (task.isSuccessful()){
                   Toast.makeText(EditPetActivity.this,
                           "You have successfully updated your pet.",
                           Toast.LENGTH_SHORT).show();
               }
           }
       });
    }

}
