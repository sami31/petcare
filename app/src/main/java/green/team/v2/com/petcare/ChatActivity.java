package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.models.Chat;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.User;
import green.team.v2.com.petcare.viewholder.MessagesViewHolder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ChatActivity";
    private static final String Error_Msg = "Something went wrong. ";
    public static final String EXTRA_RECEIVER_KEY = "receiver_key";
    private static final int Msg_tye_left = 0;
    private static final int Msg_type_right = 1;

    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter<Chat, MessagesViewHolder> messagesAdapter;
    private com.google.firebase.firestore.Query messagesQuery;

    private String receiverId;
    private EditText mMessageText;
    private ImageButton mSendBtn;
    private FirebaseAuth mAuth;
    private String senderId;
    private FirebaseUser currentUser;
    private RecyclerView messagesView;
    private String senderName;
    private String receiverName;
    private String avatarUrl;
    private String receiverPhoto;
    private LinearLayoutManager mManager;

    private Toolbar chatToolbar;
    private TextView displayTV;
    private CircleImageView displayPhoto;
    private CircleImageView displayStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        db = FirebaseFirestore.getInstance();

        receiverId = getIntent().getStringExtra(EXTRA_RECEIVER_KEY);

        chatToolbar = findViewById(R.id.chatbar_layout);
        setSupportActionBar(chatToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");


        displayTV = findViewById(R.id.display_name);
        displayPhoto = findViewById(R.id.display_picture);
        displayStatus = findViewById(R.id.display_status);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        senderId = currentUser.getUid();

        mMessageText = findViewById(R.id.messageText);
        mSendBtn = findViewById(R.id.sendMsgBtn);

        messagesView = findViewById(R.id.messages_list);
        mManager = new LinearLayoutManager(this);
        mManager.setStackFromEnd(true);
        messagesView.setLayoutManager(mManager);
        messagesView.setItemAnimator(new DefaultItemAnimator());
        displayPhoto.setOnClickListener(this);

        getReceiverProfile();
        getsenderProfile();

        messagesQuery = db.collection("messages").document(senderId).collection(receiverId)
                .orderBy("created_at", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<Chat>()
                .setQuery(messagesQuery, Chat.class)
                .setLifecycleOwner(this)
                .build();

        messagesAdapter = new FirestoreRecyclerAdapter<Chat, MessagesViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final MessagesViewHolder messagesViewHolder, int i, @NonNull Chat chat) {

                int viewType = getItemViewType(i);

                if(viewType == Msg_type_right) {
                    messagesViewHolder.message.setText(chat.chatMessage);
                }else{
                    String[] fName = chat.senderName.split(" ");
                    messagesViewHolder.receiverName.setText(fName[0]);
                    messagesViewHolder.setMessageAvatar(chat.senderPhoto);
                    messagesViewHolder.message.setText(chat.chatMessage);

                    DocumentSnapshot shot = getSnapshots().getSnapshot(i);
                    String msg_id = shot.getId();
                    updateSeenStatus(msg_id);
                }

                final TextView datetime = messagesViewHolder.datetimeTV;
                datetime.setVisibility(View.GONE);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy 'at' hh:mm:ss a", Locale.ENGLISH);
                Date date = chat.created_at;
                datetime.setText(sdf.format(date));
                messagesViewHolder.message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datetime.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                datetime.setVisibility(View.GONE);
                            }
                        }, 1500);
                    }
                });

            }

            @NonNull
            @Override
            public MessagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                if (viewType == Msg_type_right) {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_message, parent, false);
                    return new MessagesViewHolder(view);
                } else {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.their_message, parent, false);
                    return new MessagesViewHolder(view);
                }
            }

            @Override
            public int getItemViewType(int position) {
               DocumentSnapshot snapshot = getSnapshots().getSnapshot(position);
               Chat chat = snapshot.toObject(Chat.class);
                if(chat != null && chat.senderName.equals(senderName)) {
                    return Msg_type_right;
                }else {
                    return Msg_tye_left;
                }
            }
        };

        messagesView.setAdapter(messagesAdapter);

        mSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMsg();
            }
        });

    }

    private void getReceiverProfile() {
        final DocumentReference receiverRef =  db.collection("users").document(receiverId);
        receiverRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                User user = documentSnapshot.toObject(User.class);
                if (e == null && user != null){
                    receiverName = user.username;
                    displayTV.setText(receiverName);
                    receiverPhoto = user.image_url;
                    Picasso.get().load(receiverPhoto)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit().into(displayPhoto);

                    if (user.isOnline){
                        displayStatus.setBackground(ContextCompat
                                .getDrawable(ChatActivity.this, R.drawable.online_status));
                    }else{
                        displayStatus.setBackground(ContextCompat
                                .getDrawable(ChatActivity.this, R.drawable.offline_status));

                    }
                }else{
                    Log.e(TAG,"Image Url is null..");
                }

            }
        });

    }

    private void getsenderProfile(){
        DocumentReference userRef = db.collection("users").document(senderId);
        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                User user = documentSnapshot.toObject(User.class);
                if (e == null && user != null){
                    avatarUrl = user.image_url;
                    senderName = user.username;
                }
            }
        });

    }
    private void sendMsg(){

        final String mMessage;
        mMessage = mMessageText.getText().toString();

        if(TextUtils.isEmpty(mMessage)) {
            Toast.makeText(this, "Please type your message.", Toast.LENGTH_SHORT).show();
        }else {
            saveMsg(mMessage);
        }
    }

    private void saveMsg(String mMessage) {
        //create an instance of the Chat object.
        Chat chat = new Chat( senderName, receiverName, mMessage,false, avatarUrl, senderId);
        Map<String, Object> chatValues = chat.toMap();

        CollectionReference userRef =  db.collection("messages")
                .document(senderId).collection(receiverId);
        userRef.add(chatValues);
        DocumentReference userMsgRef = db.collection("users").document(senderId)
                .collection("inbox").document(receiverId);
        String myMessage = "You: " + mMessage;
        Chat chat1 = new Chat(receiverName, senderName, myMessage, false, receiverPhoto, senderId);
        userMsgRef.set(chat1);

        CollectionReference receiverRef =  db.collection("messages")
                .document(receiverId).collection(senderId);
       receiverRef.add(chatValues);
        DocumentReference receiverMsgRef = db.collection("users").document(receiverId)
                .collection("inbox").document(senderId);
        receiverMsgRef.set(chatValues);

        mMessageText.setText("");
    }

    private void updateSeenStatus(String msg_id){
        //create reference to sender and receiver inbox.
        DocumentReference userMsgRef = db.collection("users").document(senderId)
                .collection("inbox").document(receiverId);
        DocumentReference receiverMsgRef = db.collection("users").document(receiverId)
                .collection("inbox").document(senderId);

        //create HashMap for update.
        HashMap<String, Object> msgMap = new HashMap<>();
        msgMap.put("isSeen", true);

        //update inbox of msg receiver and sender.
        userMsgRef.update(msgMap);
        receiverMsgRef.update(msgMap);

        //update messages of msg receiver and sender.
        DocumentReference receiverRef = db.collection("messages").document(senderId)
                .collection(receiverId).document(msg_id);
        receiverRef.update(msgMap);

        DocumentReference senderRef = db.collection("messages").document(receiverId)
                .collection(senderId).document(msg_id);
        senderRef.update(msgMap);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.display_picture :
                Intent intent = new Intent(ChatActivity.this, PersonalInfoActivity.class);
                intent.putExtra(PersonalInfoActivity.EXTRA_USER_ID, receiverId);
                startActivity(intent);
            break;
        }
    }
}
