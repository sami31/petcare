package green.team.v2.com.petcare.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.models.Rating;
import green.team.v2.com.petcare.viewholder.ReviewsViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends androidx.fragment.app.Fragment {

    private static final String TAG = "ReviewsActivity";
    private static final String Error_Msg = "Something went wrong :( ";

    private FirebaseFirestore db;
    private String serviceproviderID;

    private LinearLayoutManager mManager;
    private RecyclerView reviewsRV;
    private FirestoreRecyclerAdapter<Rating,ReviewsViewHolder> adapter;

    public ReviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v = inflater.inflate(R.layout.fragment_reviews, container, false);

       db = FirebaseFirestore.getInstance();
       serviceproviderID = getArguments().getString("USER_KEY");

       reviewsRV = v.findViewById(R.id.allReviewsRV);
       mManager = new LinearLayoutManager(getActivity());
       mManager.setReverseLayout(false);
       mManager.setStackFromEnd(false);
       mManager.isAutoMeasureEnabled();
       reviewsRV.setLayoutManager(mManager);
       reviewsRV.setItemAnimator(new DefaultItemAnimator());
       reviewsRV.setNestedScrollingEnabled(true);

        com.google.firebase.firestore.Query reviewsQuery = db.collection("petcare_providers")
                .document(serviceproviderID).collection("ratings")
                .orderBy("rating", Query.Direction.DESCENDING).limit(10);
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<Rating>()
                .setQuery(reviewsQuery, Rating.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Rating, ReviewsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ReviewsViewHolder reviewsViewHolder, int i, @NonNull Rating rating) {
                Float reviewerRating = Float.parseFloat(rating.rating);
                String comments = '"' + rating.review + '"';

                reviewsViewHolder.reviewerName.setText(rating.rater);
                reviewsViewHolder.reviewerRating.setRating(reviewerRating);
                reviewsViewHolder.reviewerComments.setText(comments);
                reviewsViewHolder.setReviewerPhoto(rating.reviewer_photo);
            }

            @NonNull
            @Override
            public ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater viewInflater = LayoutInflater.from(parent.getContext());
                return new ReviewsViewHolder(viewInflater
                        .inflate(R.layout.item_reviews, parent, false));
            }
        };

        adapter.startListening();
        reviewsRV.setAdapter(adapter);
       return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.e(TAG, TAG);
        if (adapter != null){
            adapter.startListening();
        }
    }

}
