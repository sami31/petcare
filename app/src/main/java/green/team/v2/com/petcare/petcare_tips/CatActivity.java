package green.team.v2.com.petcare.petcare_tips;

import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.R;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class CatActivity extends AppCompatActivity {

    private ImageView kittenImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat);

        kittenImg = findViewById(R.id.kittenImg);

        kittenImg.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.cat_care));
    }
}
