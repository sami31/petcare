package green.team.v2.com.petcare.petcare_tips;

import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class DogActivity extends AppCompatActivity {

    private ImageView puppyImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog);

        puppyImg = findViewById(R.id.puppyImg);

        puppyImg.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.dog_care));

    }

}
