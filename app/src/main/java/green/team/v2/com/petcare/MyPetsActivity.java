package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.models.Pet;
import green.team.v2.com.petcare.viewholder.PetViewHolder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.HashMap;

import javax.annotation.Nullable;

public class MyPetsActivity extends AppCompatActivity {

    private static final String TAG = "My Pets Activity";
    private static final String Error_Msg = "Oops, Something went wrong :( ";
    private RecyclerView petRecyclerView;
    private GridLayoutManager mManager;
    private FirestoreRecyclerAdapter<Pet,PetViewHolder> petViewAdapter;
    private String UID;
    private FloatingActionButton addPetBtn;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pets);

         db = FirebaseFirestore.getInstance();

        Toolbar inboxToolbar = findViewById(R.id.toolbarMyPets);
        setSupportActionBar(inboxToolbar);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("My Pets");

         petRecyclerView = findViewById(R.id.petRecyclerView);
         mManager = new GridLayoutManager(this, 2);
         petRecyclerView.setHasFixedSize(true);
         petRecyclerView.setLayoutManager(mManager);
         addPetBtn = findViewById(R.id.addPetBtn);

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        UID = currentUser.getUid();

       com.google.firebase.firestore.Query petQuery = db.collection("pets")
               .whereEqualTo("owner_id", UID);

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<Pet>()
                .setLifecycleOwner(this)
                .setQuery(petQuery, Pet.class)
                .build();

        petViewAdapter = new FirestoreRecyclerAdapter<Pet, PetViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final PetViewHolder petViewHolder, int i, @NonNull Pet pet) {
                petViewHolder.petNameView.setText(pet.petName);
                String petUrl = pet.petImg;
                petViewHolder.setPetImgView(petUrl);

                final String pet_id = getSnapshots().getSnapshot(i).getId();

                petViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MyPetsActivity.this, EditPetActivity.class);
                        intent.putExtra(EditPetActivity.EXTRA_PET_ID, pet_id);
                        startActivity(intent);
                    }
                });

                Query likesRef = db.collection("pets").document(pet_id).collection("likes");

                likesRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e) {

                        if (queryDocumentSnapshots != null && e == null){
                            int likeCount = 0;
                            for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots){
                                likeCount++;
                                if (snapshot.getBoolean("isLike")) {
                                    String count;
                                    if (likeCount > 1){
                                        count = String.valueOf(likeCount) + " likes";
                                    }else {
                                        count = String.valueOf(likeCount) + " like";
                                    }
                                    petViewHolder.likes.setText(count);
                                }else {
                                    likeCount--;
                                    String count;
                                    if (likeCount > 1){
                                        count = String.valueOf(likeCount) + " likes";
                                    }else {
                                        count = String.valueOf(likeCount) + " like";
                                    }
                                    petViewHolder.likes.setText(count);
                                }

                            }
                        }

                    }
                });

                petViewHolder.likeButton.setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        updateLikes(pet_id, true);
                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        updateLikes(pet_id, false);
                    }
                });

                DocumentReference petRef = db.collection("pets").document(pet_id)
                        .collection("likes").document(UID);

                petRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null && e == null){
                            if (documentSnapshot.getBoolean("isLike" )!= null) {

                                if (documentSnapshot.getBoolean("isLike")) {

                                    petViewHolder.likeButton.setLiked(true);
                                } else {

                                    petViewHolder.likeButton.setLiked(false);
                                }
                            }
                        }
                    }
                });

            }

            @NonNull
            @Override
            public PetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new PetViewHolder(inflater.inflate(R.layout.item_pets, parent, false));
            }
        };

        addPetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPet();
            }
        });

        petRecyclerView.setAdapter(petViewAdapter);

    }

    private void addPet(){
        startActivity(new Intent(this, AddPetActivity.class));
    }

    private void updateLikes(String pet_id, Boolean like){
        final DocumentReference petRef = db.collection("pets").document(pet_id)
                .collection("likes").document(UID);
        Log.e(TAG, Error_Msg + pet_id);

        HashMap<String, Object> likeMap = new HashMap<>();
        likeMap.put("isLike", like);
        petRef.set(likeMap);

    }
}
