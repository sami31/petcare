package green.team.v2.com.petcare.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.R;

public class MessagesViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = "InboxViewHolder";
    private static final String ERROR_MSG = "Oops, Something went wrong. ";

     public TextView receiverName;
     public TextView message;
     public TextView datetimeTV;
     private View view;

    public MessagesViewHolder(@NonNull View itemView) {
        super(itemView);

        receiverName = itemView.findViewById(R.id.receiverName);
        message = itemView.findViewById(R.id.message_body);
        datetimeTV = itemView.findViewById(R.id.datetimeTV);
        view = itemView;

    }

    public void setMessageAvatar(final String messageAvatar) {
        final CircleImageView avatar = view.findViewById(R.id.avatar);
        Picasso.get().load(messageAvatar)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                .centerCrop().resize(34,34).into(avatar,
                new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Success loading image.");
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(messageAvatar).fit().into(avatar);
                        Log.e(TAG, ERROR_MSG + e);
                    }
                });
    }
}
