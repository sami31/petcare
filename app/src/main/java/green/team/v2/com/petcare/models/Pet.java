package green.team.v2.com.petcare.models;


import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Pet {

    public String owner_id;
    public String petName;
    public String petType;
    public String petBreed;
    public String petSize;
    public String petImg;
    public String petAbout;

    public Pet() {
    }

    public Pet( String owner_id, String petName, String petType,
                String petBreed, String petSize, String petImg,
                String petAbout) {

        this.owner_id   = owner_id;
        this.petName    = petName;
        this.petType    = petType;
        this.petBreed   = petBreed;
        this.petSize    = petSize;
        this.petImg     = petImg;
        this.petAbout   = petAbout;
    }

}
