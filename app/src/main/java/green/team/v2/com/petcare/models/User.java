package green.team.v2.com.petcare.models;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String username;
    public String email;
    public String contact_no;
    public String image_url;
    public String user_address;
    public Boolean isOnline;

    public User() {
        // Default constructor required for calls to DocumentSnapshot.toObject(User.class)
    }

    public User(String username, String email, String contact_no,
                String image_url, String user_address, Boolean isOnline) {

        this.username       = username;
        this.email          = email;
        this.contact_no     = contact_no;
        this.image_url      = image_url;
        this.user_address   = user_address;
        this.isOnline       = isOnline;
    }
}
