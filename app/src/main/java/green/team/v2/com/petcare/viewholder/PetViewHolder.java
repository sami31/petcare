package green.team.v2.com.petcare.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.like.LikeButton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.R;

public class PetViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = " PetViewHolder";
    private static final String ERROR_MSG = "Oops, Something went wrong. ";

    public String petImgView;
    public TextView petNameView;
    public LikeButton likeButton;
    public TextView likes;

    private View view;

    public PetViewHolder(@NonNull View itemView) {
        super(itemView);

        petNameView = itemView.findViewById(R.id.petNameView);
        likeButton = itemView.findViewById(R.id.petLikeBtn);
        likes = itemView.findViewById(R.id.noOfLikes);
        view = itemView;
    }

    public void setPetImgView(final String petImgView) {
        this.petImgView = petImgView;

        final ImageView imageView = view.findViewById(R.id.petImgView);
        Picasso.get().load(petImgView)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                .fit().into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Success loading image.");
            }

            @Override
            public void onError(Exception e) {
                Picasso.get().load(petImgView).fit().into(imageView);
                Log.e(TAG, ERROR_MSG + e);
            }
        });
    }
}
