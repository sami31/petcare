package green.team.v2.com.petcare.models;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Chat {

    @ServerTimestamp
    public Date created_at;
    public String senderName;
    public String receiverName;
    public String chatMessage;
    public Boolean isSeen;
    public String senderPhoto;
    public String sender_id;

    public Chat(){

    }

   public Chat(String senderName, String receiverName, String chatMessage,
               Boolean isSeen, String senderPhoto, String sender_id) {

        this.chatMessage    = chatMessage;
        this.isSeen         = isSeen;
        this.senderName     = senderName;
        this.receiverName   = receiverName;
        this.senderPhoto    = senderPhoto;
        this.sender_id      = sender_id;
        this.created_at     = new Date();
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("senderName", senderName);
        result.put("receiverName", receiverName);
        result.put("chatMessage", chatMessage);
        result.put("isSeen", isSeen);
        result.put("senderPhoto", senderPhoto);
        result.put("sender_id", sender_id);
        result.put("created_at", created_at);
        return result;
    }
}
