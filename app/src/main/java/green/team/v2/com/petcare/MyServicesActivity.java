package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.viewholder.PetCareViewHolder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import javax.annotation.Nullable;

public class MyServicesActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "My Services Activity";
    private static final String ERROR_MSG = "Houston, We have a problem. ";
    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter<PetCareProvider, PetCareViewHolder> adapter;

    private String UID;

    private GridLayoutManager manager;
    private RecyclerView myServiceRV;
    private TextView resultTV;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_services);

        db = FirebaseFirestore.getInstance();

        Toolbar inboxToolbar = findViewById(R.id.toolbarMyServices);
        setSupportActionBar(inboxToolbar);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("My Services");

        resultTV = findViewById(R.id.serviceResultTV);
        manager = new GridLayoutManager(this, 2);
        myServiceRV = findViewById(R.id.myServicesRV);
        myServiceRV.setHasFixedSize(true);
        myServiceRV.setLayoutManager(manager);

        FirebaseAuth mAuth;

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        UID = currentUser.getUid();

        resultTV.setVisibility(View.GONE);

        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Loading...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        getServices();
    }

    @Override
    public void onClick(View view) {
    }

    private void getServices(){
        mProgress.show();

        Query myservicesQuery = db.collection("petcare_providers")
                .whereEqualTo("user_id", UID);

        eventListener(myservicesQuery);

       FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<PetCareProvider>()
               .setQuery(myservicesQuery, PetCareProvider.class)
               .setLifecycleOwner(this)
               .build();

       adapter = new FirestoreRecyclerAdapter<PetCareProvider, PetCareViewHolder>(options) {
           @Override
           protected void onBindViewHolder(@NonNull PetCareViewHolder petCareViewHolder, int i,
                                           @NonNull PetCareProvider petCareProvider) {

               DocumentSnapshot snapshot = getSnapshots().getSnapshot(i);
               final String UID = snapshot.getId();
               final String bName = petCareProvider.serviceName;
               final String photo = petCareProvider.cover_photo;
               final String reviews = String.valueOf(petCareProvider.totalReviews);
               final Float rating = petCareProvider.avrgRating;

               petCareViewHolder.business_name.setText(bName);
               petCareViewHolder.totalReview.setText(reviews);
               petCareViewHolder.avrgRating.setRating(rating);
               petCareViewHolder.setPhoto(photo);

               petCareViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                    startActivity(new Intent(MyServicesActivity.this, EditServiceActivity.class));
                   }
               });

               resultTV.setVisibility(View.GONE);
               mProgress.dismiss();
           }

           @NonNull
           @Override
           public PetCareViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
               LayoutInflater inflater = LayoutInflater.from(parent.getContext());
               return new PetCareViewHolder(inflater.inflate
                       (R.layout.item_petcare, parent, false));
           }
       };

       myServiceRV.setAdapter(adapter);
    }

    private void eventListener(Query query){

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {

                if (e == null && queryDocumentSnapshots.isEmpty()){

                    mProgress.dismiss();
                    resultTV.setVisibility(View.VISIBLE);
                }else if(e != null){
                    Log.e(TAG,ERROR_MSG + e );
                }

            }
        });

    }
}
