package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.models.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "UserProfileActivity";
    private static final String Error_Msg = "Something went wrong :( ";
    private final int PICK_IMAGE_REQUEST = 15;


    private FirebaseFirestore db;
    private DocumentReference userRef;
    private FirebaseAuth mAuth;
    private StorageReference storageReference;
    private String UID;

    private CircleImageView userProfilePhoto;
    private ImageView userCoverPhoto;
    private EditText userName;
    private EditText userEmail;
    private EditText userContact;
    private EditText userAddress;
    private MaterialButton userSaveBtn;

    private String userPhotoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_activity);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        Toolbar inboxToolbar = findViewById(R.id.toolbarProfile);
        setSupportActionBar(inboxToolbar);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Profile");

        userProfilePhoto = findViewById(R.id.userProfilePhoto);
        userCoverPhoto = findViewById(R.id.userCoverPhoto);
        userName = findViewById(R.id.currentUserName);
        userEmail = findViewById(R.id.currentUserEmail);
        userContact = findViewById(R.id.user_Contact);
        userAddress = findViewById(R.id.currentUserAddress);
        userSaveBtn = findViewById(R.id.userSaveBtn);

        userProfilePhoto.setOnClickListener(this);
        userSaveBtn.setOnClickListener(this);
        getCurrentUser();
    }

    private void getCurrentUser(){
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null){
            UID = user.getUid();
            userRef = db.collection("users").document(UID);
            getUserProfile();
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.userProfilePhoto:
                chooseImg();
                break;

            case R.id.userSaveBtn:
                saveUser();
                break;
        }
    }

    private void getUserProfile(){

        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e == null && documentSnapshot != null){
                    User user = documentSnapshot.toObject(User.class);
                    userName.setText(user.username);
                    userEmail.setText(user.email);
                    userContact.setText(user.contact_no);
                    userAddress.setText(user.user_address);
                    Picasso.get().load(user.image_url)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit().into(userProfilePhoto);
                }else{
                    Log.e(TAG, Error_Msg + e);
                }
            }
        });
    }

    private void chooseImg(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == AddServicesActivity.RESULT_OK && data != null &&
                data.getData() != null ) {

            try{
                Uri filepath = data.getData();
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
            }catch(IOException e){
                Log.e(TAG, Error_Msg + e);
            }

        }
    }

    private void uploadImg(final byte[] bytes){
        if (bytes != null){
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();

            final StorageReference ref = storageReference.child("profile_image/" + UID + "/" + UID + ".jpg");

            UploadTask uploadTask = ref.putBytes(bytes);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(UserProfileActivity.this,
                            "File uploaded successfully", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "File uploaded successfully");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(UserProfileActivity.this,
                            Error_Msg, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, Error_Msg + e);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    float progress = (float) ((100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Upload is " + ((int)progress) + "% done");
                }
            });

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    Uri downloadUri = task.getResult();
                    userPhotoUrl = downloadUri.toString();
                    HashMap<String, Object> updates = new HashMap<>();
                    updates.put("image_url", userPhotoUrl);
                    userRef.update(updates);
                }
            });

        }
    }

    private void saveUser(){
        String name = userName.getText().toString().trim();
        String email = userEmail.getText().toString().trim();
        String contact = userContact.getText().toString().trim();
        String address = userAddress.getText().toString().trim();

        HashMap<String, Object> userUpdate = new HashMap<>();
        userUpdate.put("username", name);
        userUpdate.put("email", email);
        userUpdate.put("contact_no", contact);
        userUpdate.put("user_address", address);

        userRef.update(userUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(UserProfileActivity.this,
                            "You have successfully updated your profile.",
                            Toast.LENGTH_SHORT).show();
                }else{
                    Log.e(TAG, Error_Msg + task.getException());
                    Toast.makeText(UserProfileActivity.this,
                            "Oops! Something went wrong.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
