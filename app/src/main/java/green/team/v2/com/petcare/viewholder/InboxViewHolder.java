package green.team.v2.com.petcare.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.R;

public class InboxViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = "InboxViewHolder";
    private static final String ERROR_MSG = "Oops, Something went wrong. ";

    public TextView inboxName;
    public TextView inboxmsg;
    public TextView inboxDate;
    public CircleImageView inboxStatus;

    public InboxViewHolder(@NonNull View itemView) {
        super(itemView);
        inboxName = itemView.findViewById(R.id.inbox_name);
        inboxmsg = itemView.findViewById(R.id.inbox_msg);
        inboxDate = itemView.findViewById(R.id.inbox_date);
        inboxStatus = itemView.findViewById(R.id.inbox_status);
    }

    public void setInboxphoto(final String inboxphoto) {
        final CircleImageView photo = itemView.findViewById(R.id.inbox_photo);
        Picasso.get().load(inboxphoto)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                .fit().into(photo, new Callback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Success loading image.");
            }

            @Override
            public void onError(Exception e) {
                Picasso.get().load(inboxphoto).fit().into(photo);
                Log.e(TAG, ERROR_MSG + e);
            }
        });
    }

}
