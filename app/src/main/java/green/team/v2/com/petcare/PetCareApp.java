package green.team.v2.com.petcare;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;


import androidx.annotation.NonNull;

public class PetCareApp extends Application {

    public PetCareApp() {

    }

    private static final String TAG = "PetCare App";
    private static final String Error_Msg = "Something went wrong.";
    private final String CHANNEL_ID = "user_inbox";
    private DatabaseReference rtdb;
    private String UID;

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        rtdb = FirebaseDatabase.getInstance().getReference();

        //load pictures offline - Picasso
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            UID = currentUser.getUid();
            Log.e(TAG, "UID "+ UID);
        }

        setOnlineStatus();

        createNotificationChannel();
    }

    private void createNotificationChannel() {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "PetCare Notifications";
            String description = "New Inbox Messages";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

    }

    private void setOnlineStatus(){

        if (UID != null) {

            final DatabaseReference ref = rtdb.child("users").child(UID);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    ref.child("isOnline").onDisconnect().setValue(false);
                    ref.child("isOnline").setValue(true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, Error_Msg + databaseError);
                }
            });
        }

    }

}
