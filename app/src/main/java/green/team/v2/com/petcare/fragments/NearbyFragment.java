package green.team.v2.com.petcare.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import javax.annotation.Nullable;

import green.team.v2.com.petcare.ChatActivity;
import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.ViewProfileActivity;
import green.team.v2.com.petcare.models.PetCareProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearbyFragment extends Fragment implements OnMapReadyCallback {


    public NearbyFragment() {
        // Required empty public constructor
    }
    private static final String TAG = "Nearby Activity";
    private static final String Error_Msg = "Something went wrong.. ";
    private GoogleMap mMap;
    private MapView mapView;
    private Boolean mLocationPermissionGranted = false;
    private static final int Request_Code = 1234;
    private static final String Fine_Location = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String Coarse_Location = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final float Default_Zoom = 13f;
    private LocationCallback mLocationCallback;

    private ClusterManager<ClusterItem> mClusterManager;

    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_nearby, container, false);

        db = FirebaseFirestore.getInstance();

        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        getLocationPermission();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
            }
        };

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Marker Cluster
        setUpClusterer();
        mMap.getUiSettings().setIndoorLevelPickerEnabled(false);

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.mapstyle));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        if (mLocationPermissionGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
        }

    }

    private void getLocationPermission() {
        Log.d(TAG, "Getting permission...");

        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getContext(),
                Fine_Location) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getContext(),
                    Coarse_Location) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true;
                Log.d(TAG,"permission granted");
                initMap();
            } else {
                Log.d(TAG,"permission denied");
                ActivityCompat.requestPermissions(this.getActivity(), permission, Request_Code);
            }
        } else {
            ActivityCompat.requestPermissions(this.getActivity(), permission, Request_Code);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "called");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case Request_Code: {
                if (grantResults.length > 0) {
                    for(int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "permission failed");
                            return;
                        }
                    }
                    Log.d(TAG,"permission granted");
                    mLocationPermissionGranted = true;
                    initMap();
                }

            }
        }

    }

    private void initMap() {
        try {
            Log.d(TAG, "Initializing map...");
            mapView.getMapAsync(this);
        }catch (Exception e){
            Log.e(TAG, "Initialization failed "+ e);
        }
    }

    private void getDeviceLocation() {
        Log.d(TAG, "Getting location...");
        FusedLocationProviderClient mFusedLocationProviderClient;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.getActivity());
        try {
            if (mLocationPermissionGranted) {
                mFusedLocationProviderClient.requestLocationUpdates
                        (getLocationRequest(), mLocationCallback,
                                null /* Looper */);

                Task<Location> location = mFusedLocationProviderClient.getLastLocation();
                location.addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                      try{
                          Log.d(TAG,"Location found.");
                          Location currentLocation;
                          currentLocation = location;
                          Log.e(TAG, Error_Msg + currentLocation);
                          Log.e(TAG, Error_Msg + Default_Zoom);
                          moveCamera(new LatLng(currentLocation.getLatitude(),
                                  currentLocation.getLongitude()), Default_Zoom);
                      }catch(Exception e){
                          Log.e(TAG,"Cannot get location "+ e);
                      }
                    }
                });

            }

        } catch (SecurityException e) {
            Log.e(TAG, "Permission not granted" + e.getMessage());

        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "Moving camera...");
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    private void setUpClusterer() {

        // Initialize the manager with the context and the map.
        mClusterManager = new ClusterManager<>(getActivity(), mMap);

        // Point the map's listeners at the listeners implemented by the cluster manager.
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());

        // Setting ClusterItem On Click.
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterItem>() {
            @Override
            public boolean onClusterItemClick(ClusterItem clusterItem) {
                String title = clusterItem.getTitle();
                String UID = clusterItem.getSnippet();
                openDialog(title, UID);
                return false;
            }
        });

        // Add cluster items (markers) to the cluster manager.
        addItems();

    }

    private void openDialog(String title, final String UID) {
        CharSequence options[] = new CharSequence[]{
                "View Profile",
                "Send message"
        };

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(title);
        dialog.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i == 0) {
                    Intent profileIntent = new Intent(getActivity(), ViewProfileActivity.class);
                    profileIntent.putExtra(ViewProfileActivity.EXTRA_USER_KEY, UID);
                    startActivity(profileIntent);
                }else if (i == 1) {
                    Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
                    chatIntent.putExtra(ChatActivity.EXTRA_RECEIVER_KEY, UID);
                    startActivity(chatIntent);
                }
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog.show();
    }

    private void addItems() {
        CollectionReference petcareProviders = db.collection("petcare_providers");

        petcareProviders.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (!queryDocumentSnapshots.isEmpty()){
                    for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots){
                        final PetCareProvider provider = snapshot.toObject(PetCareProvider.class);
                        final String UID = snapshot.getId();
                        Double lat = provider.location.getLatitude();
                        Double lng = provider.location.getLongitude();
                        final LatLng latLng = new LatLng(lat,lng);

                        ClusterItem clusterItem = new ClusterItem() {
                            @Override
                            public LatLng getPosition() {
                                return latLng;
                            }

                            @Override
                            public String getTitle() {
                                return provider.serviceName;
                            }

                            @Override
                            public String getSnippet() {
                                return UID;
                            }

                        };
                        mClusterManager.addItem(clusterItem);
                    }
                }else{
                    Log.e(TAG, "no users");
                }
            }
        });
    }

}
