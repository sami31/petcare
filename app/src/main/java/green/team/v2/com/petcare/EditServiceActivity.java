package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.PetCareService;
import green.team.v2.com.petcare.models.User;
import green.team.v2.com.petcare.viewholder.ServiceViewHolder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class EditServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Add Services Activity";
    private static final String Error_Msg = "Something went wrong.";
    private static final String REQUIRED = "Required";
    private final int PICK_IMAGE_REQUEST = 15;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private StorageReference storageReference;

    private TextView petTypeTV;
    private EditText serviceNameText;
    private EditText contactNoText;
    private EditText addressText;
    private EditText aboutText;
    private ImageView coverPhotoIV;
    private List<String> servicesList;
    private List<String> petTypes;

    private RecyclerView serviceRV;
    private LinearLayoutManager manager;

    private MaterialButton editsaveBtn;
    private MaterialButton editServicesBtn;

    private String UID;
    private String coverPhoto;
    private String email;

    private Geocoder geocoder = null;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_service);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        geocoder = new Geocoder(this);

        serviceNameText = findViewById(R.id.editBName);
        contactNoText = findViewById(R.id.editcontact);
        addressText = findViewById(R.id.editaddress);
        aboutText = findViewById(R.id.editabout);
        coverPhotoIV = findViewById(R.id.editcoverphoto);
        editsaveBtn = findViewById(R.id.editSaveBtn);
        editServicesBtn = findViewById(R.id.editServicesBtn);
        serviceRV = findViewById(R.id.editserviceRV);
        petTypeTV = findViewById(R.id.editPetTypesTV);
        servicesList = new ArrayList<>();

        manager = new LinearLayoutManager(this);
        serviceRV.setHasFixedSize(true);
        serviceRV.setLayoutManager(manager);

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Updating...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        coverPhotoIV.setOnClickListener(this);
        editsaveBtn.setOnClickListener(this);
        editServicesBtn.setOnClickListener(this);
        petTypeTV.setOnClickListener(this);

        getProfile();
        displayServices();
        getBusiness();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.editSaveBtn:
                checkFields();
                break;
            case R.id.editcoverphoto:
                chooseImage();
                break;
            case R.id.editServicesBtn:
                addServicePopup();
                break;
            case R.id.editPetTypesTV:
                selectPetTypes();
                break;
        }
    }

    private void addServicePopup() {
        // Build an AlertDialog
        final Dialog dialog = new Dialog(EditServiceActivity.this);

        // Set the custom layout as alert dialog view
        dialog.setContentView(R.layout.add_service_popup);

        // Get the custom alert dialog view widgets reference
        TextView closeBtn = dialog.findViewById(R.id.closeBtn);
        MaterialButton addBtn = dialog.findViewById(R.id.addBtn);
        final Spinner servicesSpinner = dialog.findViewById(R.id.servicesSpinner);
        ArrayAdapter<CharSequence> servicesAdapter = ArrayAdapter.createFromResource(
                EditServiceActivity.this, R.array.service_category, android.R.layout.simple_spinner_dropdown_item
        );
        servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicesSpinner.setSelected(false);
        servicesSpinner.setAdapter(servicesAdapter);
        final Spinner perSpinner = dialog.findViewById(R.id.perSpinner);
        String[] per = new String[]{ "visit", "hr", "session", "night" };
        ArrayAdapter<String> perAdapter = new ArrayAdapter<>
                (EditServiceActivity.this, android.R.layout.simple_spinner_dropdown_item, per );
        perAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        perSpinner.setSelected(false);
        perSpinner.setAdapter(perAdapter);
        final EditText serviceRateText = dialog.findViewById(R.id.serviceRateET);
        final EditText detailsET = dialog.findViewById(R.id.serviceDetailsET);

        dialog.create();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String perValue = perSpinner.getSelectedItem().toString();
                final String cat = servicesSpinner.getSelectedItem().toString();
                final String serviceValue = serviceRateText.getText().toString().trim();
                final String details = detailsET.getText().toString().trim();
                final String serviceRate = serviceValue + "/" + perValue;
                editService(cat , serviceRate, details);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == AddServicesActivity.RESULT_OK && data != null &&
                data.getData() != null ) {

            try {
                Uri filepath = data.getData();
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                coverPhotoIV.setImageBitmap(bmp);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
            } catch (IOException e) {
                Log.e(TAG, Error_Msg + e);
            }
        }
    }

    private void getProfile(){
        if (currentUser != null){
            UID = currentUser.getUid();
            email = currentUser.getEmail();
        }

    }

    private void getBusiness(){
        DocumentReference ref = db.collection("petcare_providers").document(UID);
        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                PetCareProvider provider = documentSnapshot.toObject(PetCareProvider.class);
                if (e == null && provider != null) {
                    serviceNameText.setText(provider.serviceName);
                    contactNoText.setText(provider.contact_no);
                    addressText.setText(provider.address);
                    aboutText.setText(provider.about_user);
                    Picasso.get().load(provider.cover_photo)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit().into(coverPhotoIV);

                    petTypes = provider.servicePetTypes;
                        petTypeTV.setText(" ");
                    for (int i = 0; i < petTypes.size() ; i++) {

                        String item = petTypes.get(i);

                        petTypeTV.append(item);

                        if (petTypes.size() > 1 && i < ( petTypes.size() -1 )){
                            petTypeTV.append(", ");
                        }
                    }

                }else{
                    Log.e(TAG, Error_Msg + e);
                }

            }
        });

    }

    private void uploadImg(final byte[] bytedata){
        if(bytedata != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();

            final StorageReference ref = storageReference.child("cover_photo/" + UID + "/" + UUID.randomUUID().toString() + ".jpg");

            UploadTask uploadTask = ref.putBytes(bytedata);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(EditServiceActivity.this,
                            "File uploaded successfully", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "File uploaded successfully");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(EditServiceActivity.this,
                            Error_Msg, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, Error_Msg + e);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    float progress = (int)((100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Upload is " + ((int)progress) + "% done");
                }
            });

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        coverPhoto = downloadUri.toString();
                        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);
                        careProviderRef.update("cover_photo", coverPhoto);
                    }
                }
            });

        }

    }

    private void editService(final String cat, String serviceRate, String details ){

        PetCareService service = new PetCareService(cat, serviceRate, details);
        CollectionReference ref = db.collection("petcare_providers")
                .document(UID).collection("services");

        ref.add(service).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success adding a service.");
                }else{
                    Log.e(TAG, Error_Msg + task.getException());

                }
            }
        });

    }

    private void displayServices(){
        Query serviceQuery = db.collection("petcare_providers").document(UID)
                .collection("services");

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<PetCareService>()
                .setQuery(serviceQuery, PetCareService.class)
                .setLifecycleOwner(this)
                .build();

        FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder> adapter;
        adapter = new FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ServiceViewHolder holder, int i,
                                            @NonNull PetCareService service) {
                DocumentSnapshot snapshot = getSnapshots().getSnapshot(i);
                final String id = snapshot.getId();
                final String cat = service.serviceCategory;
                servicesList.add(i, cat);
                updateServicesList();

                String price = "PHP" + service.service_rate;

                holder.serviceCategory.setText(service.serviceCategory);
                holder.serviceDetails.setText(service.service_details);
                holder.service_rate.setText(price);

                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteService(id, cat);
                    }
                });

            }

            @NonNull
            @Override
            public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new ServiceViewHolder(inflater.inflate(R.layout.item_services, parent, false));
            }
        };

        serviceRV.setAdapter(adapter);

    }

    private void deleteService(String id, final String cat){
        DocumentReference ref = db.collection("petcare_providers").document(UID)
                .collection("services").document(id);

        ref.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success");
                    DocumentReference serviceRef = db.collection("petcare_providers").document(UID);
                    serviceRef.update("servicesList", FieldValue.arrayRemove(cat));
                }else{
                    Log.e(TAG, Error_Msg + task.getException());
                }
            }
        });
    }

    private void checkFields() {

        final String serviceName = serviceNameText.getText().toString().trim();
        final String contactNo = contactNoText.getText().toString().trim();
        final String address = addressText.getText().toString().trim();
        final String about_user = aboutText.getText().toString().trim();

        getLatLng(address, serviceName, contactNo, about_user);
    }

    private void getLatLng(String address, String serviceName,
                           String contactNo, String about_user ) {

        mProgress.show();

        try {
            List<Address> addressList = geocoder.getFromLocationName(address, 1);

            GeoPoint location = new GeoPoint(addressList.get(0).getLatitude(),addressList.get(0).getLongitude());

            saveService(serviceName, contactNo, address, about_user, location);

        }catch (IOException e) {
            mProgress.dismiss();
            Log.e(TAG, Error_Msg + e);
            Toast.makeText(this, "Oops, Something went wrong.", Toast.LENGTH_SHORT).show();
        }

    }

    private void saveService(
            String serviceName, final String contactNo, final String fulladdress,
            String about, GeoPoint location) {

        HashMap<String, Object> update = new HashMap<>();
        update.put("serviceName", serviceName);
        update.put("address", fulladdress);
        update.put("contact_no", contactNo);
        update.put("about_user", about);
        update.put("location", location);
        update.put("servicePetTypes", petTypes);

        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);

        careProviderRef.update(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    mProgress.dismiss();
                    Toast.makeText(EditServiceActivity.this, "You have successfully updated your services.",
                            Toast.LENGTH_SHORT).show();
                }else{
                    mProgress.dismiss();
                    Toast.makeText(EditServiceActivity.this, Error_Msg,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateServicesList(){
        HashMap<String, Object> update = new HashMap<>();
        update.put("servicesList", servicesList);
        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);
        careProviderRef.update(update);
    }

    private void selectPetTypes(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false);
        final String[] strings = getResources().getStringArray(R.array.service_pettype);

        builder.setTitle("Select pet types")
                .setMultiChoiceItems(strings, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                if (b && !petTypes.contains(strings[i])){

                                    petTypes.add(strings[i]);

                                }else if (petTypes.contains(strings[i])){

                                   petTypes.remove(strings[i]);

                                }
                            }
                        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!petTypes.isEmpty()){
                    petTypeTV.setText(" ");
                    for (int j = 0; j < petTypes.size(); j++) {

                        String item = petTypes.get(j);

                        petTypeTV.append(item);

                        if (petTypes.size() > 1 && j < (petTypes.size() - 1)){
                            petTypeTV.append(", ");
                        }
                    }
                }else{
                    Toast.makeText(EditServiceActivity.this,
                            "Please select at least one pet type.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create()
                .show();

    }

}
