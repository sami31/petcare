package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.fragments.AboutFragment;
import green.team.v2.com.petcare.fragments.ReviewsFragment;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.Rating;
import green.team.v2.com.petcare.models.User;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class ViewProfileActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {

    public static String EXTRA_USER_KEY = "user_key";
    private static final String TAG = "ViewProfileActivity";
    private static final String Error_Msg = "Something went wrong :( ";
    private FirebaseFirestore db;

    private AboutFragment aboutFragment;
    private ReviewsFragment reviewsFragment;
    private TabLayout tabs;

    private CollapsingToolbarLayout colTooolbar;
    private Toolbar toolbar;
    private AppBarLayout appBar;
    private ActionBar actionBar;

    private String serviceproviderID;
    private String UID;
    private String userName;
    private String userPhoto;
    private String user_id;

    private RatingBar avrgRatingBar;
    private TextView noOfRaters;
    private CircleImageView visitor_image;
    private TextView serviceProviderName;

    private TextView bName;
    private ImageView coverPhoto;
    private CircleImageView profilePhoto;
    private CircleImageView profileStatus;
    private MaterialButton rateBtn;
    private FloatingActionButton chatBtn;
    private ImageView nextIV;

    private Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        db = FirebaseFirestore.getInstance();

        serviceproviderID = getIntent().getStringExtra(EXTRA_USER_KEY);

        aboutFragment = new AboutFragment();
        reviewsFragment = new ReviewsFragment();
        tabs = findViewById(R.id.tabs);

        appBar = findViewById(R.id.appBar);
        colTooolbar = findViewById(R.id.collapsingToolBar);
        toolbar = findViewById(R.id.toolBar);

        bName = findViewById(R.id.businessName);
        coverPhoto = findViewById(R.id.coverPhoto);
        profilePhoto = findViewById(R.id.profilePhoto);
        rateBtn = findViewById(R.id.rate);
        chatBtn = findViewById(R.id.chatBtn);
        avrgRatingBar = findViewById(R.id.avrg_ratingBar);
        noOfRaters = findViewById(R.id.no_of_raters);
        serviceProviderName = findViewById(R.id.serviceProviderName);
        profileStatus = findViewById(R.id.profileStatus);
        nextIV = findViewById(R.id.nextIV);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        b = new Bundle();
        b.putString("USER_KEY", serviceproviderID);

        tabs.addOnTabSelectedListener(this);
        rateBtn.setOnClickListener(this);
        chatBtn.setOnClickListener(this);
        nextIV.setOnClickListener(this);

        setFragment(aboutFragment);
        getRating();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if(user != null){
            UID = user.getUid();
            getUser();
        }

    }

    private void getUser(){
        //get current user profile.
        DocumentReference ref = db.collection("users").document(UID);
        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null && documentSnapshot != null) {
                    User user = documentSnapshot.toObject(User.class);
                    userPhoto = user.image_url;
                    userName = user.username;
                    getProfile();
                }

            }
        });
    }

    private void getProfile(){
        //get profile of petcare provider then update the UI.
        db.collection("petcare_providers").document(serviceproviderID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                        @javax.annotation.Nullable FirebaseFirestoreException e) {
                        final PetCareProvider provider = documentSnapshot.toObject(PetCareProvider.class);
                        if(e == null && provider != null){
                            bName.setText(provider.serviceName);
                            user_id = provider.user_id;
                            Picasso.get().load(provider.cover_photo)
                                    .networkPolicy(NetworkPolicy.OFFLINE).fit().into(coverPhoto);
                            getProviderProfile();

                            appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                                boolean isShow = true;
                                int scrollRange;

                                @Override
                                public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                                    scrollRange = appBarLayout.getTotalScrollRange();

                                    if (scrollRange + i == 0){
                                        colTooolbar.setTitle(provider.serviceName);
                                        isShow = true;
                                    }else if(isShow){
                                        colTooolbar.setTitle(" ");
                                        isShow = false;
                                    }
                                }
                            });

                        }else{
                            Log.e(TAG,Error_Msg + e);
                        }

                    }
                });
    }

    private void getProviderProfile(){
        db.collection("users").document(serviceproviderID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        final User user = documentSnapshot.toObject(User.class);
                        if (user != null){
                            serviceProviderName.setText(user.username);
                            Picasso.get().load(user.image_url)
                                    .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                                    .fit().into(profilePhoto, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.d(TAG, "ok");
                                }

                                @Override
                                public void onError(Exception e) {
                                    Picasso.get().load(user.image_url).fit().into(profilePhoto);
                                    Log.e(TAG, Error_Msg + e);
                                }
                            });

                            if (user.isOnline){
                                profileStatus.setBackground(ContextCompat
                                        .getDrawable(ViewProfileActivity.this , R.drawable.online_status));
                            }else{
                                profileStatus.setBackground(ContextCompat
                                        .getDrawable(ViewProfileActivity.this, R.drawable.offline_status));

                            }
                        }
                    }
                });
    }

    private void getRating(){
        // get reference to petcare provider ratings.
        CollectionReference ratingRef = db.collection("petcare_providers").document(serviceproviderID)
                .collection("ratings");

        //listen to value events of the rating then compute for average rating and no. of reviews.
        ratingRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {
                double totalRating = 0;
                Integer count = 0;
                double average;

                if (queryDocumentSnapshots != null && e == null){

                    for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots){
                        Rating rating = snapshot.toObject(Rating.class);
                        Log.e(TAG, "rater "+ rating.rater);

                        double userRating = Double.parseDouble(rating.rating);
                        totalRating = totalRating + userRating;
                        count = count + 1;
                        average = totalRating / count;

                        setRating(average, count);

                    }
                }else{
                    Log.e(TAG, Error_Msg + e);
                }
            }
        });

    }

    private void setRating(Double average, Integer count){
        //convert average to String.
        String aVerage = String.valueOf(average);

        //convert average to Float.
        Float avrg = Float.parseFloat(aVerage);

        //update the UI
        avrgRatingBar.setRating(avrg);
        String raters = String.valueOf(count);
        noOfRaters.setText(raters);

        //get reference to petcare provider.
        DocumentReference ref = db.collection("petcare_providers")
                .document(serviceproviderID);

        //update average rating and no. of reviews on database.
        HashMap<String, Object> updates = new HashMap<>();
        updates.put("totalReviews", count);
        updates.put("avrgRating", average);
        ref.update(updates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()){
                    Log.e(TAG, Error_Msg + task.getException());
                }
            }
        });

    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragment.setArguments(b);
        fragmentTransaction.replace(R.id.profileFL, fragment);
        fragmentTransaction.commit();

    }

    private void chatIntent(){
        //create intent for chat activity
        Intent chatIntent  = new Intent(
                ViewProfileActivity.this,
                ChatActivity.class
        );

        //put petcare provider ID on intent then start intent.
        chatIntent.putExtra(ChatActivity.EXTRA_RECEIVER_KEY, user_id );
        startActivity(chatIntent);
    }

    private void personalInfoIntent(){
        //create intent for personal info activity
        Intent personalInfoIntent  = new Intent(
                ViewProfileActivity.this,
                PersonalInfoActivity.class

        );

        //put user_id on intent then start intent.
        personalInfoIntent.putExtra(PersonalInfoActivity.EXTRA_USER_ID, user_id );
        startActivity(personalInfoIntent);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){

          case R.id.rate:
              ratingDialog();
              break;

          case R.id.chatBtn:
              chatIntent();
              break;

          case R.id.nextIV:
              personalInfoIntent();
              break;

        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Integer pos = tab.getPosition();
        switch(pos){
            case 0:
                setFragment(aboutFragment);
                break;
            case 1:
                setFragment(reviewsFragment);
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Integer pos = tab.getPosition();
        switch(pos){
            case 0:
                setFragment(aboutFragment);
                break;
            case 1:
                setFragment(reviewsFragment);
                break;
        }
    }

    private void ratingDialog(){
        // Build an AlertDialog
        final Dialog dialog = new Dialog(ViewProfileActivity.this);

        // Set the custom layout as alert dialog view
        dialog.setContentView(R.layout.rating_popup);

        // Get the custom alert dialog view widgets reference
        Button saveBtn = dialog.findViewById(R.id.ratingBtn);
        TextView closeBtn = dialog.findViewById(R.id.closeRatingBtn);
        final RatingBar ratingBar = dialog.findViewById(R.id.visitor_rating);
        final EditText user_review = dialog.findViewById(R.id.visitor_reviews);
        visitor_image = dialog.findViewById(R.id.visitor_image);
        Picasso.get().load(userPhoto)
                .networkPolicy(NetworkPolicy.OFFLINE).fit().into(visitor_image);

        //set the background as transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // Create the alert dialog
        dialog.create();

        // Set save button click listener
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String review = user_review.getText().toString().trim();
                Float rating = ratingBar.getRating();
                submitRating(review,rating);

                // Dismiss the alert dialog
                dialog.dismiss();
            }
        });

        //Set close button click listener
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        // Display the custom alert dialog on interface
        dialog.show();
    }

    private void submitRating(String review, Float user_rating){
        String rate = String.valueOf(user_rating);
        CollectionReference ratingRef = db.collection("petcare_providers").document(serviceproviderID)
                .collection("ratings");
        Rating rating = new Rating(userName, rate, review, userPhoto);
        ratingRef.document(UID).set(rating).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Successfully added a rating");
                }else{
                    Log.e(TAG, Error_Msg + task.getException());
                }
            }
        });
    }
}
