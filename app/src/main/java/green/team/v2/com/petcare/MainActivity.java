package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.fragments.NearbyFragment;
import green.team.v2.com.petcare.fragments.SearchFragment;
import green.team.v2.com.petcare.fragments.TipsFragment;
import green.team.v2.com.petcare.models.Chat;
import green.team.v2.com.petcare.models.User;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.URL;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main Activity";
    private static final String ERROR_MSG = "Something went wrong. ";
    private BottomNavigationView bottomNav;
    private FrameLayout mainFrame;
    private Toolbar mainToolbar;
    private DrawerLayout mDrawerLayout;
    private CircleImageView profilePic;
    private ImageView siderbar_cover;
    private  NavigationView navigationView;
    private TextView UserName;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private NearbyFragment nearbyFragment;
    private SearchFragment searchFragment;
    private TipsFragment tipsFragment;

    private String UID;
    private String userName;
    private Boolean serviceExists;
    private boolean doubleBackToExitPressedOnce = false;
    private final String CHANNEL_ID = "user_inbox";
    private final int notificationId = 15;
    private int new_msg_count = 0;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        bottomNav = findViewById(R.id.bottomNav);
        mainFrame = findViewById(R.id.mainFrame);
        mainToolbar = findViewById(R.id.main_toolbar);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        setSupportActionBar(mainToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_account_circle_black_24dp);
        actionBar.setTitle("PetCare");

        nearbyFragment = new NearbyFragment();
        searchFragment = new SearchFragment();
        tipsFragment = new TipsFragment();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.drawer_profile:
                        startActivity(new Intent(MainActivity.this, UserProfileActivity.class));
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.drawer_services:
                        startServiceIntent(serviceExists);
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.drawer_logout:
                        logOut();
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.drawer_pets:
                        startActivity(new Intent( getBaseContext() , MyPetsActivity.class ));
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.drawer_policy:
                        startActivity(new Intent(getBaseContext(), PrivacyPolicyActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.drawer_inbox:
                        startActivity(new Intent(getBaseContext(), InboxActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;

                    default: return false;
                }
            }
        });

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_nearby:
                        setFragment(nearbyFragment);
                        return true;

                    case R.id.nav_search:
                        setFragment(searchFragment);
                        return true;

                    case R.id.nav_tips:
                        setFragment(tipsFragment);
                        return true;

                    default: return false;
                }
            }
        });

        setFragment(nearbyFragment);
        displayProfile();
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();

    }

    private void logOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         super.onCreateOptionsMenu(menu);
       getMenuInflater().inflate(R.menu.main_menu, menu);

       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayProfile() {
        if( currentUser != null ){
            UID = currentUser.getUid();
            final DocumentReference userDoc =  db.collection("users").document(UID);
            userDoc.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                    @Nullable FirebaseFirestoreException e) {
                    User user = documentSnapshot.toObject(User.class);

                    if(e == null && user != null) {
                        userName = user.username;
                        final String profileURL = user.image_url;
                        profilePic = navigationView.getHeaderView(0).findViewById(R.id.profile_pic);
                        UserName = navigationView.getHeaderView(0).findViewById(R.id.UserName);
                        siderbar_cover = navigationView.getHeaderView(0).findViewById(R.id.sidebar_cover);
                        Picasso.get().load(profileURL)
                                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                                .fit().into(profilePic, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.d(TAG, "ok");
                            }

                            @Override
                            public void onError(Exception e) {
                                Picasso.get().load(profileURL).fit().into(profilePic);
                                Log.e(TAG, ERROR_MSG + e);
                            }
                        });
                        UserName.setText(userName);
                        checkServiceIfExists();
                        getNewMessages();
                    }else{
                        Log.e(TAG, ERROR_MSG + e);
                    }
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        mDrawerLayout.closeDrawers();
        if (doubleBackToExitPressedOnce) {
            exitDialog();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    private void exitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("PetCare")
                .setMessage("Are you sure you want to exit the app?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create();
        builder.show();

    }

    private void getNewMessages(){
        CollectionReference ref = db.collection("users").document(UID)
                .collection("inbox");
        ref.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null && queryDocumentSnapshots != null){
                   for (DocumentSnapshot snapshot: queryDocumentSnapshots){
                       Chat chat = snapshot.toObject(Chat.class);
                        if (chat != null && !chat.isSeen && !chat.sender_id.equals(UID)){
                            String name = chat.senderName;
                            String msg = chat.chatMessage;
                            new_msg_count = new_msg_count + 1;
                            updateBadge();
                            displayNotification(name, msg);
                        }
                   }
                }else{
                    Log.e(TAG, ERROR_MSG + e);
                }
            }
        });
    }

   private void updateBadge(){
        Menu navMenu = navigationView.getMenu();
        MenuItem item = navMenu.findItem(R.id.drawer_inbox);
        TextView inbox =(TextView) item.getActionView();
        String count = String.valueOf(new_msg_count);
        SpannableString msgs = new SpannableString(count);

        if (new_msg_count > 0) {
            inbox.setVisibility(View.VISIBLE);
            inbox.setText(msgs);
        }else{
            inbox.setVisibility(View.GONE);
        }
   }

    public void displayNotification( String senderName, String msg ){

        Intent intent = new Intent(this, InboxActivity.class);
        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_petcarelogo_colored);
        builder.setContentTitle(senderName);
        builder.setContentText(msg);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        builder.setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, builder.build());

    }

    private void checkServiceIfExists(){

        DocumentReference ref = db.collection("petcare_providers").document(UID);
        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null){
                    if (documentSnapshot != null){
                        serviceExists = true;
                    }else{
                        serviceExists = false;
                    }
                }else{
                    Log.e(TAG, ERROR_MSG + e);
                }
            }
        });
    }

    private void startServiceIntent(Boolean exists){
        if (exists){
            startActivity(new Intent(MainActivity.this, EditServiceActivity.class ));
        }else{
            startActivity(new Intent(MainActivity.this, AddServicesActivity.class ));
        }
    }
}
