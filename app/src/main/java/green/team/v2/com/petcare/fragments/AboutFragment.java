package green.team.v2.com.petcare.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.PetCareService;
import green.team.v2.com.petcare.viewholder.ServiceViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends androidx.fragment.app.Fragment implements OnMapReadyCallback{

    private static final String TAG = "AboutActivity";
    private static final String Error_Msg = "Something went wrong :( ";
    private static final float Default_Zoom = 12;

    private GoogleMap mMap;
    private MapView mapView;

    private FirebaseFirestore db;
    private String serviceproviderID;

    private FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder> adapter;
    private RecyclerView petcareSerivceRV;
    private LinearLayoutManager manager;

    private TextView addressTV;
    private TextView petTYpeTV;
    private TextView aboutTV;
    private LatLng latLng;

    private String bName;
    private String cat;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_about, container, false);

        serviceproviderID = getArguments().getString("USER_KEY");

        db = FirebaseFirestore.getInstance();

        petcareSerivceRV = v.findViewById(R.id.petcareServices);

        manager = new LinearLayoutManager(getActivity());
        petcareSerivceRV.setHasFixedSize(true);
        petcareSerivceRV.setLayoutManager(manager);

        mapView = v.findViewById(R.id.petCareMapView);
        mapView.onCreate(savedInstanceState);

        addressTV = v.findViewById(R.id.addressTV);
        petTYpeTV = v.findViewById(R.id.petTypeTV);
        aboutTV = v.findViewById(R.id.about_user);
        aboutTV.setSingleLine(false);
        getProfile();
        getServices();
        return v;
    }

    private void getProfile(){
        DocumentReference ref = db.collection("petcare_providers").document(serviceproviderID);
        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null && documentSnapshot != null) {
                    PetCareProvider provider = documentSnapshot.toObject(PetCareProvider.class);
                    bName = provider.serviceName;
                    addressTV.setText(provider.address);
                    aboutTV.setText(provider.about_user);
                    Double lat = provider.location.getLatitude();
                    Double lng = provider.location.getLongitude();
                    latLng = new LatLng(lat, lng);
                    initMap();
                    petTYpeTV.setText(" ");

                    for (int i = 0; i < provider.servicePetTypes.size(); i++) {

                        String item = provider.servicePetTypes.get(i);

                        petTYpeTV.append(item);
                        if (provider.servicePetTypes.size() > 1
                                && i < ( provider.servicePetTypes.size() - 1 )){

                            petTYpeTV.append(", ");
                        }
                    }
                }else{
                    Log.e(TAG, Error_Msg + e);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.mapstyle));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        mMap.addMarker(new MarkerOptions().position(latLng)
        .title(bName).snippet(cat));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, Default_Zoom));
    }

    private void initMap(){
        mapView.getMapAsync(this);

    }

    private void getServices(){
        Query q = db.collection("petcare_providers").document(serviceproviderID)
                .collection("services");

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<PetCareService>()
                .setQuery(q, PetCareService.class)
                .setLifecycleOwner(this)
                .build();

        adapter = new FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ServiceViewHolder holder, int i, @NonNull PetCareService service) {
                DocumentSnapshot snapshot = getSnapshots().getSnapshot(i);
                final String id = snapshot.getId();

                String price = "PHP" + service.service_rate;

                holder.serviceCategory.setText(service.serviceCategory);
                holder.serviceDetails.setText(service.service_details);
                holder.service_rate.setText(price);
                holder.deleteBtn.setVisibility(View.GONE);
            }

            @NonNull
            @Override
            public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new ServiceViewHolder(inflater.inflate(R.layout.item_services, parent, false));
            }
        };

        petcareSerivceRV.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }
}
