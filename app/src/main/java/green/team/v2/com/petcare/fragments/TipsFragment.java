package green.team.v2.com.petcare.fragments;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;

import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.petcare_tips.CatActivity;
import green.team.v2.com.petcare.petcare_tips.DogActivity;
import green.team.v2.com.petcare.petcare_tips.RabbitActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TipsFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "Sign-In Activity";
    private static final String Error_Msg = "Something went wrong.";
    private CardView dogCardView;
    private CardView catCardView;
    private CardView rabbitCardView;

    private ImageView dogImg;
    private ImageView catImg;
    private ImageView rabbitImg;

    private DogActivity dogActivity;
    private CatActivity catActivity;
    private RabbitActivity rabbitActivity;

    public TipsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_tips, container, false);

        dogCardView = rootview.findViewById(R.id.dogCardView);
        catCardView = rootview.findViewById(R.id.catCardView);
        rabbitCardView = rootview.findViewById(R.id.rabbitCardView);

        dogImg = rootview.findViewById(R.id.dogImg);
        catImg = rootview.findViewById(R.id.catImg);
        rabbitImg = rootview.findViewById(R.id.rabbitImg);

        dogImg.setImageBitmap( getBitmap(R.drawable.dog) );
        catImg.setImageBitmap( getBitmap(R.drawable.cat) );
        rabbitImg.setImageBitmap( getBitmap(R.drawable.rabbit) );

        dogActivity = new DogActivity();
        catActivity = new CatActivity();
        rabbitActivity = new RabbitActivity();

        dogCardView.setOnClickListener(this);
        catCardView.setOnClickListener(this);
        rabbitCardView.setOnClickListener(this);

        return rootview;

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.dogCardView:
                startActivity(dogActivity);
                break;

            case R.id.catCardView:
                startActivity(catActivity);
                break;

            case R.id.rabbitCardView:
                startActivity(rabbitActivity);
                break;
        }
    }

    private void startActivity(Activity activity){
        startActivity( new Intent( getActivity(), activity.getClass() ) );
    }

    private Bitmap getBitmap(int i){
         Bitmap bmp =  BitmapFactory.decodeResource(
                getResources(), i);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
        byte[] bytedata = baos.toByteArray();
        Bitmap decoded = BitmapFactory.decodeByteArray(bytedata, 0, bytedata.length);
        return decoded;
    }

}
