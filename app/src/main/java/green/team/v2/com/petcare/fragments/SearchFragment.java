package green.team.v2.com.petcare.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import javax.annotation.Nullable;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.ChatActivity;
import green.team.v2.com.petcare.R;
import green.team.v2.com.petcare.ViewProfileActivity;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.viewholder.PetCareViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {


    public SearchFragment() {
        // Required empty public constructor
    }

    private static final String TAG = "Search Fragment";
    private static final String ERROR_MSG = "Houston, We have a problem. ";
    private ProgressBar bar;
    private GridLayoutManager mManager;
    private RecyclerView searchRV;
    private FirebaseFirestore db;

    private FirestoreRecyclerAdapter<PetCareProvider, PetCareViewHolder> adapter;
    private Query searchQuery;
    private FirestoreRecyclerOptions options;

    private Spinner categorySpinner;
    private SearchView searchV;
    private TextView resultTv;
    private String cat;
    private String initQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        db = FirebaseFirestore.getInstance();
        bar = view.findViewById(R.id.searchProgress);
        bar.setVisibility(View.GONE);

        resultTv = view.findViewById(R.id.resultTV);
        resultTv.setVisibility(View.GONE);

        categorySpinner = view.findViewById(R.id.categorySpinner);
        ArrayAdapter<CharSequence> catAdptr = ArrayAdapter.createFromResource(
                getActivity(), R.array.service_category, android.R.layout.simple_spinner_dropdown_item
        );
        catAdptr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setSelected(false);
        categorySpinner.setAdapter(catAdptr);

        searchRV = view.findViewById(R.id.recyclerView);

        mManager = new GridLayoutManager(getActivity(), 2);
        searchRV.setLayoutManager(mManager);
        searchRV.setItemAnimator(new DefaultItemAnimator());

        searchV = view.findViewById(R.id.searchV);
        searchV.setIconifiedByDefault(false);

        searchV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);
                return true;
            }
        });

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                search(searchV.getQuery().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                initQuery = searchV.getQuery().toString();
                search(initQuery);
            }
        });

        return view;
    }

    private void openDialog(final String UID, final String userName ) {
        CharSequence options[] = new CharSequence[]{
                "View Profile",
                "Send message"
        };

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle(userName);
        dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i == 0) {
                    Intent profileIntent = new Intent(getActivity(), ViewProfileActivity.class);
                    profileIntent.putExtra(ViewProfileActivity.EXTRA_USER_KEY, UID);
                    startActivity(profileIntent);

                }else if (i == 1) {
                    Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
                    chatIntent.putExtra(ChatActivity.EXTRA_RECEIVER_KEY, UID);
                    startActivity(chatIntent);

                }
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialogBuilder.show();
    }

    private void search( String query ){
        bar.setVisibility(View.VISIBLE);
        resultTv.setVisibility(View.GONE);
        cat = categorySpinner.getSelectedItem().toString().trim();

        if (!query.isEmpty()){

            int index = query.length();
            String frontCode =  query.substring(0, index - 1);
            String strEndCode = query.substring(index - 1, index);
            String endCode = frontCode + (char)(((int)strEndCode.charAt(0)) + 1);

            searchQuery = db.collection("petcare_providers")
                    .whereGreaterThanOrEqualTo("serviceName", query)
                    .whereLessThan("serviceName", endCode)
                    .whereArrayContains("servicesList", cat);

            setAdapter();

        }else{
            searchQuery = db.collection("petcare_providers")
                    .orderBy("avrgRating", Query.Direction.DESCENDING);

            setAdapter();
        }
    }

    private void setAdapter(){

        queryListener();

        options = new FirestoreRecyclerOptions.Builder<PetCareProvider>()
                .setQuery(searchQuery, PetCareProvider.class)
                .setLifecycleOwner(this)
                .build();

        adapter = new FirestoreRecyclerAdapter<PetCareProvider, PetCareViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull PetCareViewHolder petCareViewHolder, int i,
                                            @NonNull PetCareProvider petCareProvider) {

                DocumentSnapshot snapshot = getSnapshots().getSnapshot(i);
                final String id = snapshot.getId();
                final String bName = petCareProvider.serviceName;
                final String photo = petCareProvider.cover_photo;
                final String reviews = String.valueOf(petCareProvider.totalReviews);
                final Float rating = petCareProvider.avrgRating;
                final List<String> servicesList = petCareProvider.servicesList;

                bar.setVisibility(View.GONE);
                resultTv.setVisibility(View.GONE);

                petCareViewHolder.business_name.setText(bName);
                petCareViewHolder.totalReview.setText(reviews);
                petCareViewHolder.avrgRating.setRating(rating);
                petCareViewHolder.setPhoto(photo);
                petCareViewHolder.category.setText(" ");
                if (servicesList != null) {

                    for (int j = 0; j < servicesList.size(); j++) {
                        String service = servicesList.get(j);
                        petCareViewHolder.category.append(service);

                        if (servicesList.size() > 1 && j < (servicesList.size() - 1)) {
                            petCareViewHolder.category.append(", ");
                        }
                    }
                }

                petCareViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(id, bName);
                    }
                });

            }

            @NonNull
            @Override
            public PetCareViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new PetCareViewHolder(inflater.inflate
                        (R.layout.item_petcare, parent, false));
            }
        };

        searchRV.setAdapter(adapter);
    }

    private void queryListener(){

        searchQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {

                if (e == null && queryDocumentSnapshots.isEmpty()){
                    resultTv.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.GONE);
                }else if ( e != null ){
                    String msg = "Something went wrong.";
                    resultTv.setText(msg);
                    resultTv.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.GONE);
                    Log.e(TAG, ERROR_MSG + e);
                }

            }
        });

    }
}
