package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.models.Chat;
import green.team.v2.com.petcare.models.User;
import green.team.v2.com.petcare.viewholder.InboxViewHolder;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.ref.Reference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Nullable;

public class InboxActivity extends AppCompatActivity {

    private static final String TAG = "Inbox Activity";
    private static final String ERROR_MSG = "Houston, We have a problem. ";
    private String UID;

    private FirebaseFirestore db;

    private RecyclerView inboxRV;
    private LinearLayoutManager manager;
    private FirestoreRecyclerAdapter<Chat, InboxViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        UID = mAuth.getCurrentUser().getUid();

        Toolbar inboxToolbar = findViewById(R.id.toolbarInbox);
        setSupportActionBar(inboxToolbar);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Inbox");

        inboxRV = findViewById(R.id.inboxRV);
        manager = new LinearLayoutManager(this);
        inboxRV.setHasFixedSize(true);
        inboxRV.setLayoutManager(manager);

        Query inboxQuery = db.collection("users").document(UID).collection("inbox");

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<Chat>()
                .setQuery(inboxQuery, Chat.class)
                .setLifecycleOwner(this)
                .build();

        adapter = new FirestoreRecyclerAdapter<Chat, InboxViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final InboxViewHolder holder, int i, @NonNull final Chat chat) {
                final String id = getSnapshots().getSnapshot(i).getId();
                holder.inboxName.setText(chat.senderName);
                holder.setInboxphoto(chat.senderPhoto);
                holder.inboxmsg.setText(chat.chatMessage);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy 'at' hh:mm:ss a",
                        Locale.ENGLISH);
                Date date = chat.created_at;
                holder.inboxDate.setText(sdf.format(date));

                if (!chat.isSeen) {
                    holder.inboxmsg.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorBack));
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getBaseContext(), ChatActivity.class);
                        intent.putExtra(ChatActivity.EXTRA_RECEIVER_KEY, id);
                        startActivity(intent);
                    }
                });

                DocumentReference ref = db.collection("users").document(id);
                ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null && e == null){
                            User user = documentSnapshot.toObject(User.class);
                            if (user.isOnline){
                                holder.inboxStatus.setBackground(ContextCompat
                                        .getDrawable(InboxActivity.this, R.drawable.online_status));
                            }else{
                                holder.inboxStatus.setBackground(ContextCompat
                                        .getDrawable(InboxActivity.this, R.drawable.offline_status));
                            }
                        }else{
                            Log.e(TAG, ERROR_MSG + e);
                        }
                    }
                });
            }

            @NonNull
            @Override
            public InboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new InboxViewHolder(inflater.inflate(R.layout.item_inbox, parent, false));
            }
        };

        inboxRV.setAdapter(adapter);
    }
}
