package green.team.v2.com.petcare.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import green.team.v2.com.petcare.R;

public class ReviewsViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = " PetViewHolder";
    private static final String ERROR_MSG = "Oops, Something went wrong. ";

    public TextView reviewerName;
    public TextView reviewerComments;
    public RatingBar reviewerRating;
    private View v;

    public ReviewsViewHolder(@NonNull View itemView) {
        super(itemView);

        reviewerName = itemView.findViewById(R.id.reviewer_name);
        reviewerComments = itemView.findViewById(R.id.reviewer_comments);
        reviewerRating = itemView.findViewById(R.id.reviewer_ratingBar);
        v = itemView;
    }

    public void setReviewerPhoto(final String reviewerPhoto) {
        final CircleImageView photo = v.findViewById(R.id.reviewer_photo);
        Picasso.get().load(reviewerPhoto)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                .fit().into(photo,
                new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Success loading image.");
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(reviewerPhoto).fit().into(photo);
                        Log.e(TAG, ERROR_MSG + e);
                    }
                });
    }
}
