package green.team.v2.com.petcare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import green.team.v2.com.petcare.models.PetCareProvider;
import green.team.v2.com.petcare.models.PetCareService;
import green.team.v2.com.petcare.models.User;
import green.team.v2.com.petcare.viewholder.ServiceViewHolder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firestore.v1beta1.Document;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ref.Reference;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class AddServicesActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Add Services Activity";
    private static final String Error_Msg = "Something went wrong.";
    private static final String REQUIRED = "Required";
    private final int PICK_IMAGE_REQUEST = 15;
    private final int IMAGE_CAPTURE_REQUEST = 31;

    private RecyclerView serviceRV;
    private LinearLayoutManager manager;

    private MaterialButton submitBtn;
    private MaterialButton addServicesBtn;
    private LinearLayout coverPhotoLV;

    private Spinner citySpinner;
    private EditText serviceNameText;
    private EditText contactNoText;
    private EditText addressText;
    private EditText aboutText;
    private ImageView coverPhotoIV;
    private TextView petTypeTV;
    private List<String> servicesList;
    private List<String> petTypes;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private StorageReference storageReference;

    private String UID;
    private String username;
    private String email;
    private String photo_url;
    private String coverPhoto;

    private Geocoder geocoder = null;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_services);

        citySpinner = findViewById(R.id.citySpinner);
        ArrayAdapter<CharSequence> cityAdapter = ArrayAdapter.createFromResource(
                this, R.array.cities_towns,android.R.layout.simple_spinner_dropdown_item);

        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setSelected(false);
        citySpinner.setAdapter(cityAdapter);

        petTypeTV = findViewById(R.id.petTypesTV);
        coverPhotoLV = findViewById(R.id.coverPhotoLV);
        serviceNameText = findViewById(R.id.serviceName);
        contactNoText = findViewById(R.id.contact_no);
        addressText = findViewById(R.id.address);
        aboutText = findViewById(R.id.about);
        coverPhotoIV = findViewById(R.id.profileCover);
        submitBtn = findViewById(R.id.submitBtn);
        addServicesBtn = findViewById(R.id.addServicesBtn);
        serviceRV = findViewById(R.id.servicesRV);
        servicesList = new ArrayList<>();
        petTypes = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        geocoder = new Geocoder(this);

        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Saving...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        coverPhotoIV.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        addServicesBtn.setOnClickListener(this);
        petTypeTV.setOnClickListener(this);

        manager = new LinearLayoutManager(this);
        serviceRV.setHasFixedSize(true);
        serviceRV.setLayoutManager(manager);

        getCurrentUser();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submitBtn:
                checkFields();
                break;
            case R.id.profileCover:
                chooseImage();
                break;
            case R.id.addServicesBtn:
                addServicePopup();
                break;
            case R.id.petTypesTV:
                selectPetTypes();
                break;
        }
    }

    private void addServicePopup(){
        // Build an AlertDialog
        final Dialog dialog = new Dialog(AddServicesActivity.this);

        // Set the custom layout as alert dialog view
        dialog.setContentView(R.layout.add_service_popup);

        // Get the custom alert dialog view widgets reference
        TextView closeBtn = dialog.findViewById(R.id.closeBtn);
        MaterialButton addBtn = dialog.findViewById(R.id.addBtn);
        final Spinner servicesSpinner = dialog.findViewById(R.id.servicesSpinner);
        ArrayAdapter<CharSequence> servicesAdapter = ArrayAdapter.createFromResource(
                this, R.array.service_category, android.R.layout.simple_spinner_dropdown_item);
        servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicesSpinner.setSelected(false);
        servicesSpinner.setAdapter(servicesAdapter);
        final Spinner perSpinner = dialog.findViewById(R.id.perSpinner);
        String[] per = new String[]{ "visit", "hr", "session", "night" };
        ArrayAdapter<String> perAdapter = new ArrayAdapter<>
                (AddServicesActivity.this, android.R.layout.simple_spinner_item, per );
        perAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        perSpinner.setSelected(false);
        perSpinner.setAdapter(perAdapter);
        final EditText serviceRateText = dialog.findViewById(R.id.serviceRateET);
        final EditText detailsET = dialog.findViewById(R.id.serviceDetailsET);

        dialog.create();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String perValue = perSpinner.getSelectedItem().toString();
                final String cat = servicesSpinner.getSelectedItem().toString();
                final String serviceValue = serviceRateText.getText().toString().trim();
                final String details = detailsET.getText().toString().trim();
                final String serviceRate = serviceValue + "/" + perValue;
                addService(cat , serviceRate, details);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void addService( final String cat, String serviceRate, String details ){
        final PetCareService service = new PetCareService(cat, serviceRate, details);

        CollectionReference ref = db.collection("petcare_providers")
                .document(UID).collection("services");

        ref.add(service).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success adding a service.");
                }else{
                    Log.e(TAG, Error_Msg + task.getException());

                }
            }
        });

    }

    private void getCurrentUser() {
        UID = currentUser.getUid();
        username = currentUser.getDisplayName();
        email = currentUser.getEmail();
        photo_url = currentUser.getPhotoUrl().toString();
        displayServices();
        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);
        careProviderRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {
                PetCareProvider careProvider = documentSnapshot.toObject(PetCareProvider.class);
                if (careProvider != null && careProvider.cover_photo != null){
                    Picasso.get().load(careProvider.cover_photo)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit().into(coverPhotoIV);
                    coverPhotoLV.setVisibility(View.INVISIBLE);
                }

            }
        });
    }

    private void showPicDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select action.");
        CharSequence[] options = {
                "Select photo from gallery.",
                "Capture photo from camera."
        };

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0){
                    chooseImage();
                }else if (i == 1){
                    takePhotoFromCamera();
                }
            }
        });

        builder.show();

    }

    private void chooseImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void takePhotoFromCamera(){
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, IMAGE_CAPTURE_REQUEST);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == AddServicesActivity.RESULT_OK && data != null &&
                data.getData() != null ) {

            try{
                Uri filepath = data.getData();
                Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                coverPhotoIV.setImageBitmap(bmp);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
            }catch(IOException e){
                Log.e(TAG, Error_Msg + e);
            }

        }else if (requestCode == IMAGE_CAPTURE_REQUEST && resultCode == RESULT_OK
        && data != null && data.getData() != null){
                Log.e(TAG, "Camera intent");
                Bundle extras = data.getExtras();
                Bitmap bmp = (Bitmap) extras.get("data");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
                byte[] bytedata = baos.toByteArray();
                uploadImg(bytedata);
        }
    }

    private void uploadImg(final byte[] bytedata){
        if(bytedata != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();

            final StorageReference ref = storageReference.child("cover_photo/" + UID + "/" + UUID.randomUUID().toString() + ".jpg");

            UploadTask uploadTask = ref.putBytes(bytedata);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(AddServicesActivity.this,
                            "File uploaded successfully", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "File uploaded successfully");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(AddServicesActivity.this,
                            Error_Msg, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, Error_Msg + e);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    float progress = (int)((100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Upload is " + ((int)progress) + "% done");
                }
            });

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        coverPhoto = downloadUri.toString();
                        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);
                        careProviderRef.update("cover_photo", coverPhoto);
                    }
                }
            });

        }
    }

    private void checkFields() {

        final String serviceName = serviceNameText.getText().toString().trim();
        final String contactNo = contactNoText.getText().toString().trim();
        final String address = addressText.getText().toString().trim();
        final String about_user = aboutText.getText().toString().trim();
        final String city = citySpinner.getSelectedItem().toString().trim();

        String[] values = new String[]{serviceName, contactNo, address, about_user};
        EditText[] fields = new EditText[]{serviceNameText, contactNoText, addressText, aboutText};

        for(int i = 0; i < values.length; i++){

            if(values[i].isEmpty()){
                fields[i].setError(REQUIRED);
                Toast.makeText(this, "Please fill up required fields.",
                        Toast.LENGTH_SHORT).show();
                return;
            }

        }

        final String fulladdress = address + ", " + city;

        mProgress.show();

        getLatLng(fulladdress, serviceName, contactNo, about_user);
    }

    private void saveService(
            String serviceName, final String contactNo, final String fulladdress,
            String about, GeoPoint location) {

        PetCareProvider petCareProvider = new PetCareProvider( UID,
                serviceName, email, fulladdress, contactNo, petTypes,
                location, about, coverPhoto, 0, 0f, servicesList
        );
        User user = new User(username, email, contactNo, photo_url, fulladdress, true);

        DocumentReference careProviderRef  = db.collection("petcare_providers").document(UID);
        DocumentReference userRef =  db.collection("users").document(UID);

        userRef.set(user);
        careProviderRef.set(petCareProvider).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    mProgress.dismiss();
                    Toast.makeText(AddServicesActivity.this, "You are now a PetCare Provider",
                            Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddServicesActivity.this, SplashActivity.class));
                    finish();

                } else {
                    mProgress.dismiss();
                    Log.e(TAG, Error_Msg + task.getException());
                    Toast.makeText(AddServicesActivity.this, "Oops, Somethig went wrong.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getLatLng(String address, String serviceName,
                           String contactNo, String about_user ) {

        try {
           List<Address> addressList = geocoder.getFromLocationName(address, 1);

           GeoPoint location = new GeoPoint(addressList.get(0).getLatitude(),addressList.get(0).getLongitude());

           saveService(serviceName, contactNo, address, about_user, location);

        }catch (IOException e) {
            mProgress.dismiss();
            Log.e(TAG, Error_Msg + e);
            Toast.makeText(this, Error_Msg + "Oops, Something went wrong.", Toast.LENGTH_SHORT).show();
        }

    }

    private void displayServices(){
        Query serviceQuery = db.collection("petcare_providers").document(UID)
                .collection("services");

        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<PetCareService>()
                .setQuery(serviceQuery, PetCareService.class)
                .setLifecycleOwner(this)
                .build();

        FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder> adapter;
        adapter = new FirestoreRecyclerAdapter<PetCareService, ServiceViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ServiceViewHolder holder, int i,
                                            @NonNull PetCareService service) {
                DocumentSnapshot snapshot = getSnapshots().getSnapshot(i);
                final String id = snapshot.getId();
                final String cat = service.serviceCategory;
                servicesList.add( i, cat );

                String price = "PHP" + service.service_rate;

                holder.serviceCategory.setText(service.serviceCategory);
                holder.serviceDetails.setText(service.service_details);
                holder.service_rate.setText(price);

                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteService( id, cat );
                    }
                });

            }

            @NonNull
            @Override
            public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                return new ServiceViewHolder(inflater.inflate(R.layout.item_services, parent, false));
            }
        };

        serviceRV.setAdapter(adapter);

    }

    private void deleteService(String id, final String cat){
        DocumentReference ref = db.collection("petcare_providers").document(UID)
                .collection("services").document(id);

        ref.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success");
                    DocumentReference serviceRef = db.collection("petcare_providers").document(UID);
                    serviceRef.update("servicesList", FieldValue.arrayRemove(cat));
                }else{
                    Log.e(TAG, Error_Msg + task.getException());
                }
            }
        });

    }

    private void selectPetTypes(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final String[] strings = getResources().getStringArray(R.array.service_pettype);

        builder.setTitle("Select pet types")
                .setMultiChoiceItems(strings, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {

                        if (b && !petTypes.contains(strings[i])){

                            petTypes.add(strings[i]);

                        }else if (petTypes.contains(strings[i])){

                            petTypes.remove(strings[i]);

                        }
                    }
                });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (petTypes.size() > 0){
                    petTypeTV.setText(" ");

                    for (int j = 0; j < petTypes.size(); j++) {

                        String item = petTypes.get(j);
                        petTypeTV.append(item);

                        if (petTypes.size() > 1 && j < ( petTypes.size() - 1 )){
                            petTypeTV.append(", ");
                        }
                    }
                }else{
                    Toast.makeText(AddServicesActivity.this,
                            "Please select at least one pet type.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.create()
                .show();

    }

}
