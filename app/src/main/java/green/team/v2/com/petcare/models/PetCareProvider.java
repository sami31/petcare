package green.team.v2.com.petcare.models;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.Arrays;
import java.util.List;

@IgnoreExtraProperties
public class PetCareProvider {

    public String user_id;
    public String address;
    public String contact_no;
    public String email;
    public String serviceName;
    public List<String> servicePetTypes;
    public GeoPoint location;
    public String about_user;
    public String cover_photo;
    public Integer totalReviews;
    public Float avrgRating;
    public List<String> servicesList;

    public PetCareProvider() {
    }

    public PetCareProvider(String user_id, String serviceName, String email, String address, String contact_no,
                           List<String> servicePetTypes, GeoPoint location, String about_user, String cover_photo,
                            Integer totalReviews, Float avrgRating, List<String> servicesList) {

        this.user_id            = user_id;
        this.serviceName        = serviceName;
        this.email              = email;
        this.address            = address;
        this.contact_no         = contact_no;
        this.servicePetTypes     = servicePetTypes;
        this.location           = location;
        this.about_user         = about_user;
        this.cover_photo        = cover_photo;
        this.totalReviews       = totalReviews;
        this.avrgRating         = avrgRating;
        this.servicesList       = servicesList;
    }


}
