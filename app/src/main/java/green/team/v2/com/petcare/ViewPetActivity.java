package green.team.v2.com.petcare;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import green.team.v2.com.petcare.models.Pet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import javax.annotation.Nullable;

public class ViewPetActivity extends AppCompatActivity {

    private static final String TAG = "ViewPetActivity";
    private static final String Error_Msg = "Oops, Something went wrong :( ";
    public static final String EXTRA_PET_ID = "pet_key";

    private FirebaseFirestore db;
    private ActionBar actionBar;

    private String pet_id;
    private String user_id;
    private String CUID;

    private ImageView petIV;
    private TextView petName, petType, petBreed, petSize, petAbout, likesCount;
    private LikeButton likeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pet);

        db = FirebaseFirestore.getInstance();

        pet_id = getIntent().getStringExtra(EXTRA_PET_ID);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.petToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        petIV = findViewById(R.id.petPhoto);
        petName = findViewById(R.id.petNameTV);
        petType = findViewById(R.id.petTypeTv);
        petBreed = findViewById(R.id.petBreedTV);
        petSize = findViewById(R.id.petSizeTV);
        petAbout = findViewById(R.id.petAboutTV);
        likesCount = findViewById(R.id.likesCountTV);
        likeButton = findViewById(R.id.pet_like_btn);

        getPetInfo();

        likeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                updateLikes(pet_id, true);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                updateLikes(pet_id, false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            CUID = currentUser.getUid();
            getLikeStatus();
        }
    }

    private void getPetInfo(){

        DocumentReference ref = db.collection("pets").document(pet_id);

        ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null && e == null){
                    Pet pet = documentSnapshot.toObject(Pet.class);
                    user_id = pet.owner_id;
                    actionBar.setTitle(pet.petName);
                    petName.setText(pet.petName);
                    petType.setText(pet.petType);
                    petBreed.setText(pet.petBreed);
                    petSize.setText(pet.petSize);
                    petAbout.setText(pet.petAbout);
                    Picasso.get().load(pet.petImg)
                            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE)
                            .fit()
                            .into(petIV);
                    getLikes();
                }else{
                    Log.e(TAG, Error_Msg + e);

                }
            }
        });
    }

    private void getLikes(){

        Query likesRef = db.collection("pets").document(pet_id).collection("likes");

        likesRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {

                if (queryDocumentSnapshots != null && e == null){
                    int likeCount = 0;
                    for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots){
                        likeCount++;
                        if (snapshot.getBoolean("isLike")) {
                            String count;
                            if (likeCount > 1){
                                count = String.valueOf(likeCount) + " likes";
                            }else {
                                count = String.valueOf(likeCount) + " like";
                            }
                            likesCount.setText(count);
                        }else {
                            likeCount--;
                            String count;
                            if (likeCount > 1){
                                count = String.valueOf(likeCount) + " likes";
                            }else {
                                count = String.valueOf(likeCount) + " like";
                            }
                            likesCount.setText(count);
                        }

                    }
                }
            }
        });

    }

    private void getLikeStatus(){

        DocumentReference petRef = db.collection("pets").document(pet_id)
                .collection("likes").document(CUID);

        petRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null && e == null){
                    if (documentSnapshot.getBoolean("isLike" )!= null) {

                        if (documentSnapshot.getBoolean("isLike")) {
                            likeButton.setLiked(true);
                        } else {
                            likeButton.setLiked(false);
                        }
                    }
                }
            }
        });

    }

    private void updateLikes(String pet_id, Boolean like){
        final DocumentReference petRef = db.collection("pets").document(pet_id)
                .collection("likes").document(CUID);
        Log.e(TAG, Error_Msg + pet_id);

        HashMap<String, Object> likeMap = new HashMap<>();
        likeMap.put("isLike", like);
        petRef.set(likeMap);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ViewPetActivity.this, PersonalInfoActivity.class);
                intent.putExtra(PersonalInfoActivity.EXTRA_USER_ID, user_id);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ViewPetActivity.this, PersonalInfoActivity.class);
        intent.putExtra(PersonalInfoActivity.EXTRA_USER_ID, user_id);
        startActivity(intent);
        finish();
    }
}
