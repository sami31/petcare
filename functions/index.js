const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const firestore = admin.firestore();

exports.onUserStatusChange = functions.database
	.ref('/users/{uid}')
	.onUpdate((change, context) => {
		
		var db = admin.firestore();
		
		
		//const usersRef = firestore.document('/users/' + event.params.userId);
        const usersRef = firestore.doc(`users/${context.params.uid}`);
		// var snapShot = change.after.val();
		
		return change.after.ref.once('value').then((statusSnapshot) => {
            const status = statusSnapshot.val();
            console.log(status.isOnline);

            const online = status.isOnline;
    
            if(online == false){
                
                usersRef.update({
                    isOnline: false
                });
                
            }else{
                usersRef.update({
                    isOnline: true
                });
            }

          });
                    
});